//
//  LanguageVC.swift
//  Brexa
//
//  Created by Jarvics on 31/10/17.
//  Copyright © 2017 Omninos_Solutions. All rights reserved.
//

import UIKit
import Gemini
class LanguageVC: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    var picwomen : NSArray = ["ENGLISH","HINDI","MALAYALAM","ARABIC"]
    
    @IBOutlet var LangCollectionView: GeminiCollectionView!
    @IBOutlet var MainView: UIView!
   // @IBOutlet var LangCollectionView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()

        MainView.layer.shadowColor = UIColor.lightGray.cgColor
        MainView.layer.shadowOffset = CGSize(width:0,height: 1.5)
        MainView.layer.shadowRadius = 2.0
        MainView.layer.shadowOpacity = 1.0
      LangCollectionView.gemini.customAnimation().translation(y: 50).rotationAngle(y: 13).ease(.easeOutExpo).shadowEffect(.fadeIn).maxShadowAlpha(0.3)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override var prefersStatusBarHidden : Bool {
        return true
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
           return picwomen.count
       
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
       let cell = LangCollectionView.dequeueReusableCell(withReuseIdentifier: "LangCollectionViewCell", for: indexPath)as! LangCollectionViewCell
        cell.langimage.image = UIImage(named: picwomen[indexPath.row] as! String)
        
            return cell
    
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

            let cellsize = CGSize( width:self.LangCollectionView.frame.size.width/1.4 , height:self.LangCollectionView.frame.size.height)
            return cellsize


        }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "WalkScreens") as! WalkScreens
        self.navigationController?.pushViewController(vc, animated: true)
    }
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
//        let totalCellWidth = 80 * LangCollectionView.numberOfItems(inSection: 0)
//        let totalSpacingWidth = 10 * (LangCollectionView.numberOfItems(inSection: 0) - 1)
//
//        let leftInset = (LangCollectionView.layer.frame.size.width - CGFloat(totalCellWidth + totalSpacingWidth)) / 2
//        let rightInset = leftInset
//
//        return UIEdgeInsetsMake(0, leftInset, 0, rightInset)
//
//    }
}
