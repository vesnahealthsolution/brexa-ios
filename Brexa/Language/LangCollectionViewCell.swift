//
//  LangCollectionViewCell.swift
//  Brexa
//
//  Created by Jarvics on 31/10/17.
//  Copyright © 2017 Omninos_Solutions. All rights reserved.
//

import UIKit

class LangCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var langimage: UIImageView!
}
