//
//  WalkScreens.swift
//  Brexa
//
//  Created by Jarvics on 31/10/17.
//  Copyright © 2017 Omninos_Solutions. All rights reserved.
//

import UIKit

class WalkScreens: UIViewController,UIScrollViewDelegate {
    @IBOutlet var ScrollView: UIScrollView!
    
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet var PageController: UIPageControl!
    @IBOutlet var ThrdView: UIView!
    @IBOutlet var FrstView: UIView!
    @IBOutlet var SecView: UIView!
      var a = Int()
    override func viewDidLoad() {
        super.viewDidLoad()

        ScrollView.contentSize = CGSize(width: self.view.frame.size.width * 3, height:0)
        ScrollView.contentOffset = CGPoint(x: 0, y: 0)
        self.bottomView.addSubview(PageController)
        self.PageController.numberOfPages = 3
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func changePage(sender: AnyObject) -> () {
        let x = CGFloat(PageController.currentPage) * ScrollView.frame.size.width
        ScrollView.setContentOffset(CGPoint(x:x, y:0), animated: true)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageNumber = round(ScrollView.contentOffset.x / ScrollView.frame.size.width)
        
        a = Int(pageNumber)
        
        PageController.currentPage = Int(pageNumber)
        
    }
    

    @IBAction func Skip(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PlayintroVC") as! PlayintroVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func Nextbtn(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PlayintroVC") as! PlayintroVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
