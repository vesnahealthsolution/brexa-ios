//
//  Global.swift
//  Aappii
//
//  Created by Jarvics on 19/05/17.
//  Copyright © 2017 Omninos_Solutions. All rights reserved.
//

import UIKit
import Alamofire

class Global: NSObject {
    
    let BaseURL = "http://vesnahealthsolutions.com/api/"
        
    func showSimpleAlert( Title : String, message: String , inClass : UIViewController) {
        let alertController = UIAlertController(title: Title, message:message, preferredStyle: UIAlertControllerStyle.alert)
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default,handler: nil))
        inClass.present(alertController, animated: true) {
            
        }
    }
    
    func alamofire_withMethod(_ method: String, parameters : NSDictionary, completion: @escaping (_ result : AnyObject) -> (), failure : @escaping (_ reason : AnyObject)->()) {
        
        Alamofire.request("\(BaseURL)\(method)", method: .post ,parameters: parameters as? Parameters).responseJSON { response in
            
            if let JSON = response.result.value {
                if let _ = JSON as? NSArray{
                    let result : NSArray = (JSON as? NSArray)!
                    completion(result)
                }else if let _ = JSON as? NSDictionary{
                    let result : NSDictionary = (JSON as? NSDictionary)!
                    completion(result)
                }
            }else{
                print(response)
                failure(response.result.value as AnyObject)
            }
        }
    }
    func alamofire_withGetMethod(_ method: String, parameters : NSDictionary, completion: @escaping (_ result : AnyObject) -> (), failure : @escaping (_ reason : AnyObject)->()) {
        
        Alamofire.request("\(BaseURL)\(method)", method: .get ,parameters: parameters as? Parameters).responseJSON { response in
            
            if let JSON = response.result.value {
                if let _ = JSON as? NSArray{
                    let result : NSArray = (JSON as? NSArray)!
                    completion(result)
                }else if let _ = JSON as? NSDictionary{
                    let result : NSDictionary = (JSON as? NSDictionary)!
                    completion(result)
                }
            }else{
                print(response)
                failure(response.result.value as AnyObject)
            }
        }
    }
    
    func alamofire_withMethod10( method: String, parameters : NSDictionary, completion: @escaping ( _ result : AnyObject) -> (), failure: @escaping (_ fail : AnyObject) -> ()) {
        
        Alamofire.request("\(BaseURL)\(method)", method: .post ,parameters: parameters as? Parameters).responseString { response in
            if let JSON = response.result.value {
                if let _ = JSON as? NSString{
                    let result : String = (JSON as? NSString as! String)
                    completion(result as AnyObject)
                }else if let _ = JSON as? NSString{
                    let result : NSString = (JSON as? NSString)!
                    completion(result)
                }
            }else{
                failure(response as AnyObject)
                print(response)
            }
        }
    }    
    func createPost(method: String, StringParameters : NSDictionary? = nil, additionalArrayParameters : NSDictionary? = nil, imagesDataArray : NSMutableArray, completion: @escaping (_ result : AnyObject) -> ()) {
        Alamofire.upload(
            multipartFormData: { MultipartFormData in
                
                var x = 0
                while x < imagesDataArray.count{
                   
                    let imgData : Data = imagesDataArray[x] as! Data
                    MultipartFormData.append(imgData, withName: "image[]", fileName: "image\(x).jpg", mimeType: "image/jpg")
                    x += 1
                }
                
                if StringParameters != nil{
                    for (key, value) in StringParameters! {
                        MultipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key as! String)
                    }
                }
                if additionalArrayParameters != nil{
                    for (key, value) in additionalArrayParameters! {
                        for item in (value as? NSArray)! {
                            MultipartFormData.append((item as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key as! String)
                        }
                    }
                }
                
        }, to: "\(BaseURL)\(method)") { (result) in
            
            switch result {
            case .success(let upload,_,_):
                
                upload.uploadProgress { progress in
                    //                    let uploading = Int((progress.fractionCompleted).roundTo(places: 2) * 100)
                    //                    print(Int((progress.fractionCompleted).roundTo(places: 2) * 100))
                    //                    NotificationCenter.default.post(name: Notification.Name("uploadProgress"), object: uploading)
                }
                upload.responseJSON { response in
                    print("\nresponse :\(response)\n")
                    completion(response.result.value as AnyObject)
                }
                
            case .failure(let encodingError):
                print(encodingError)
            }
        }
    }
    func alamofire_uploadImage1( method:String,parameters: NSDictionary,myimage: Data,imagewithname: String, completion:@escaping ( _ result : AnyObject) -> ()) {
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            multipartFormData.append(myimage, withName: imagewithname,fileName: "file.jpg", mimeType: "image/jpg")
            for (key, value) in parameters {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key as! String)
            }
        },
                         to: "\(BaseURL)\(method)") { (result) in
                            
                            switch result {
                            case .success(let upload, _,_):
                                
                                upload.uploadProgress { progress in
                                    let uploading = Int((progress.fractionCompleted).roundTo(places: 2) * 100)
                                    print(Int((progress.fractionCompleted).roundTo(places: 2) * 100))
                                    NotificationCenter.default.post(name: Notification.Name("uploadProgress"), object: uploading)
                                }
                                
                                upload.responseJSON { response in
                                    
                                    print(response)
                                    completion(response.result.value as AnyObject)
                                    //                        NotificationCenter.default.post(name: Notification.Name("refreshPosts"), object: nil)
                                }
                                
                            case .failure(let encodingError):
                                print(encodingError)
                            }
        }
    }
    func alamofire_uploadImage(_ method:String,parameters: NSDictionary,myimage: Data,imagewithname: String, completion:@escaping (_ result : AnyObject) -> ()) {
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            multipartFormData.append(myimage, withName: imagewithname,fileName: "file.jpg", mimeType: "image/jpg")
            
            for (key, value) in parameters {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key as! String)
            }
        },
                         to: "\(BaseURL)\(method)") { (result) in
                            
                            switch result {
                            case .success(let upload, _, _):
                                
                                upload.uploadProgress { progress in
                                    let uploading = Int((progress.fractionCompleted).roundTo(places: 2) * 100)
                                    print(Int((progress.fractionCompleted).roundTo(places: 2) * 100))
                                    NotificationCenter.default.post(name: Notification.Name("uploadProgress"), object: uploading)
                                }
                                
                                upload.responseJSON { response in
                                    
                                    print(response)
                                    completion(response.result.value as AnyObject)
                                    //                        NotificationCenter.default.post(name: Notification.Name("refreshPosts"), object: nil)
                                }
                                
                            case .failure(let encodingError):
                                print(encodingError)
                            }
        }
    }
    
    //Send Data in Multipart form with dictionary and images
    func createPost(method: String, parameters : NSDictionary? = nil, imagesDataArray : NSArray,audio : NSData? = nil, fileType : String, fileExtension : String, completion: @escaping (_ result : AnyObject) -> ()) {
        Alamofire.upload(
            multipartFormData: { MultipartFormData in
                
                var fileXT = String()
                var mimeType = String()
                fileXT = "jpg"
                mimeType = "image/jpg"
                
                
                //         print("\(fileType!)PostFile.\(fileXT)")
                //         print(mimeType)
                
                var x = 0
                while x < imagesDataArray.count{
                    let imgData : Data = imagesDataArray[x] as! Data
                    print("\n\n\n")
                    print(imgData.count)
                    print(imgData)
                    
                    
                    MultipartFormData.append(imgData, withName: "userFiles[]", fileName: "\(fileType)PostFile.\(fileXT)", mimeType: mimeType)
                    x += 1
                }
                
                //String Parameters
                if parameters != nil{
                    for (key, value) in parameters! {
                        MultipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key as! String)
                    }
                }
                
        }, to: "\(BaseURL)\(method)") { (result) in
            
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress { progress in
                    let uploading = Int((progress.fractionCompleted).roundTo(places: 2) * 100)
                    print(Int((progress.fractionCompleted).roundTo(places: 2) * 100))
                    NotificationCenter.default.post(name: Notification.Name("uploadProgress"), object: uploading)
                }
                
                upload.responseJSON { response in
                    
                    print(response)
                    completion(response.result.value as AnyObject)
                    //                        NotificationCenter.default.post(name: Notification.Name("refreshPosts"), object: nil)
                }
                
            case .failure(let encodingError):
                print(encodingError)
            }
        }
    }
    //Send Data in Multipart form with dictionary and images
    func createProfile(method: String, parameters : NSDictionary? = nil, imagesDataArray : NSArray,audio : NSData? = nil, fileType : String, fileExtension : String, completion: @escaping (_ result : AnyObject) -> ()) {
        Alamofire.upload(
            multipartFormData: { MultipartFormData in
                
                //                var fileXT = String()
                //                var mimeType = String()
                //                if fileType == "video"{
                //                    fileXT = fileExtension
                //                    mimeType = "video/\(fileExtension)"
                //                }else{
                //                    fileXT = "jpg"
                //                    mimeType = "image/jpg"
                //                }
                
                //         print("\(fileType!)PostFile.\(fileXT)")
                //         print(mimeType)
                
                var x = 0
                while x < imagesDataArray.count{
                    
                    
                    MultipartFormData.append((imagesDataArray[x] as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: "product_id[\(x)]")
                    
                    
                    x += 1
                }
                
                //String Parameters
                if parameters != nil{
                    for (key, value) in parameters! {
                        MultipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key as! String)
                    }
                }
                
                //                //Audio
                //                if audio != nil{
                //                    MultipartFormData.append(audio as! Data, withName: "audio", fileName: "userAudioFiles.mp3", mimeType: "audio/mp3")
                //                }else{
                //                    let value = ""
                //                    MultipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: "audio")
                //                }
                
        }, to: "\(BaseURL)\(method)") { (result) in
            
            switch result {
            case .success(let upload,_,_):
                
                upload.uploadProgress { progress in
                    let uploading = Int((progress.fractionCompleted).roundTo(places: 2) * 100)
                    print(Int((progress.fractionCompleted).roundTo(places: 2) * 100))
                    NotificationCenter.default.post(name: Notification.Name("uploadProgress"), object: uploading)
                }
                
                upload.responseJSON { response in
                    
                    print(response)
                    completion(response.result.value as AnyObject)
                    //                        NotificationCenter.default.post(name: Notification.Name("refreshPosts"), object: nil)
                }
                
            case .failure(let encodingError):
                print(encodingError)
            }
        }
    }
    func createPostimage(method: String, parameters : NSDictionary? = nil, imagesDataArray : NSArray,audio : NSData? = nil, fileType : String, fileExtension : String, completion: @escaping (_ result : AnyObject) -> ()) {
        Alamofire.upload(
            multipartFormData: { MultipartFormData in
                
                var fileXT = String()
                var mimeType = String()
                if fileType == "video"{
                    fileXT = fileExtension
                    mimeType = "video/\(fileExtension)"
                }else{
                    fileXT = "jpg"
                    mimeType = "image/jpg"
                }
                
                //         print("\(fileType!)PostFile.\(fileXT)")
                //         print(mimeType)
                
                var x = 0
                while x < imagesDataArray.count{
                    let imgData : Data = imagesDataArray[x] as! Data
                    print("\n\n\n")
                    print(imgData.count)
                    print(imgData)
                    
                    
                    MultipartFormData.append(imgData, withName: "image[]", fileName: "\(fileType)PostFile.\(fileXT)", mimeType: mimeType)
                    x += 1
                }
                
                //String Parameters
                if parameters != nil{
                    for (key, value) in parameters! {
                        MultipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key as! String)
                    }
                }
                
                //Audio
                if audio != nil{
                    MultipartFormData.append(audio as! Data, withName: "audio", fileName: "userAudioFiles.mp3", mimeType: "audio/mp3")
                }else{
                    let value = ""
                    MultipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: "audio")
                }
                
        }, to: "\(BaseURL)\(method)") { (result) in
            
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress { progress in
                    let uploading = Int((progress.fractionCompleted).roundTo(places: 2) * 100)
                    print(Int((progress.fractionCompleted).roundTo(places: 2) * 100))
                    NotificationCenter.default.post(name: Notification.Name("uploadProgress"), object: uploading)
                }
                
                upload.responseJSON { response in
                    
                    print(response)
                    completion(response.result.value as AnyObject)
                    //                        NotificationCenter.default.post(name: Notification.Name("refreshPosts"), object: nil)
                }
                
            case .failure(let encodingError):
                print(encodingError)
            }
        }
    }
    func makeprofile(method: String, StringParameters : NSDictionary? = nil, additionalArrayParameters : NSDictionary? = nil, imagesDataArray : NSArray, completion: @escaping (_ result : AnyObject) -> ()) {
        Alamofire.upload(
            multipartFormData: { MultipartFormData in
                
                var x = 0
                while x < imagesDataArray.count{
                    let imgData : Data = imagesDataArray[x] as! Data
                    MultipartFormData.append(imgData, withName: "image[]", fileName: "image\(x).jpg", mimeType: "image/jpg")
                    x += 1
                }
                
                if StringParameters != nil{
                    for (key, value) in StringParameters! {
                        MultipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key as! String)
                    }
                }
                if additionalArrayParameters != nil{
                    for (key, value) in additionalArrayParameters! {
                        for item in (value as? NSArray)! {
                            MultipartFormData.append((item as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key as! String)
                        }
                    }
                }
                
        }, to: "\(BaseURL)\(method)") { (result) in
            
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress { progress in
                    //                    let uploading = Int((progress.fractionCompleted).roundTo(places: 2) * 100)
                    //                    print(Int((progress.fractionCompleted).roundTo(places: 2) * 100))
                    //                    NotificationCenter.default.post(name: Notification.Name("uploadProgress"), object: uploading)
                }
                upload.responseJSON { response in
                    print("\nresponse :\(response)\n")
                    completion(response.result.value as AnyObject)
                }
                
            case .failure(let encodingError):
                print(encodingError)
            }
        }
    }
    //MARK:- Create Base64String from image
    func generateBase64ImageString(image : UIImage) -> String {
        let ImageData: NSData = UIImageJPEGRepresentation(image, 0.1)! as NSData
        let ImageString: String = ImageData.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        return ImageString
    }
    
   
}


extension Double {
    /// Rounds the double to decimal places value
    func roundTo(places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}


