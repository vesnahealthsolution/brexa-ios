//
//  breastselfExamViewController.swift
//  Brexa
//
//  Created by Jarvics  on 20/12/17.
//  Copyright © 2017 Omninos_Solutions. All rights reserved.
//

import UIKit
import AVFoundation

class breastselfExamViewController: UIViewController {
    var global = Bool()

    @IBOutlet weak var webView1: UIWebView!
    var player:AVPlayer?
    @IBOutlet weak var playImg: UIImageView!
    @IBOutlet weak var pauseBtn: UIButton!
    @IBOutlet weak var playBtn: UIButton!
    @IBOutlet weak var forwardBtn: UIButton!
    @IBOutlet weak var rewindBtn: UIButton!
    @IBOutlet var IncreaseView: UIView!
    @IBOutlet var Screenimg: UIImageView!
    @IBOutlet var headerView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

         self.view.backgroundColor = UIColor(red: 187/255.0, green: 42/255.0, blue: 105/255.0, alpha: 1)
        playImg.image = UIImage(named :"pause")
        playBtn.isHidden = true
        pauseBtn.isHidden = false
        
        let videoURL = URL(string: "http://vesnahealthsolutions.com/video/english/breastselfexamination.mp4")
        player = AVPlayer(url: videoURL!)
        let playerLayer = AVPlayerLayer(player: player)
        playerLayer.frame = self.view.bounds
        self.view.layer.addSublayer(playerLayer)
        player?.play()
    }
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }

    @IBAction func backBtn(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func skipbtn(_ sender: Any) {
        
    }
    @IBAction func Fullscreenbtn(_ sender: Any) {
        if global == true{
            IncreaseView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.size.height)
            Screenimg.image = UIImage(named:"Smallscreen")
            global = false
        }else{
            IncreaseView.frame = CGRect(x: self.headerView.frame.origin.x, y: self.headerView.frame.origin.y + self.headerView.frame.size.height, width: self.view.frame.width, height: self.view.frame.size.height)
            Screenimg.image = UIImage(named:"Fullscreen")
            global = true
        }
    }
    
    @IBAction func playPauseBtn(_ sender: UIButton) {
        
        if sender.tag == 0
        {
          initPlayer()
            playImg.image = UIImage(named :"pause")
            playBtn.isHidden = true
            pauseBtn.isHidden = false
        }
        
        if sender.tag == 1
        {
          stopPlayer()
            playImg.image = UIImage(named :"play")
            playBtn.isHidden = false
            pauseBtn.isHidden = true
        }
    }
    
    func initPlayer()  {
//        if let play = player {
//            print("playing")
//            play.play()
//        } else {
//            print("player allocated")
//            player = AVPlayer(url: NSURL(string: "http://vesnahealthsolutions.com/video/english/breastselfexamination.mp4")! as URL)
//            print("playing")
//            player!.play()
//        }
        
        let videoURL = URL(string: "http://vesnahealthsolutions.com/video/english/breastselfexamination.mp4")
        player = AVPlayer(url: videoURL!)
        let playerLayer = AVPlayerLayer(player: player)
        playerLayer.frame = self.view.bounds
        self.view.layer.addSublayer(playerLayer)
        player?.play()
    }
    
    func stopPlayer() {
        if let play = player {
            print("stopped")
            play.pause()
            player = nil
            print("player deallocated")
        } else {
            print("player was already deallocated")
        }
    }


}
