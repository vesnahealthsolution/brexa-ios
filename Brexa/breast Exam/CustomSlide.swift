//
//  CustomSlide.swift
//  Brexa
//
//  Created by Jarvics  on 19/12/17.
//  Copyright © 2017 Omninos_Solutions. All rights reserved.
//

import UIKit

class CustomSlide: UISlider {

    @IBInspectable var trackHeight: CGFloat = 2
    
    override func trackRect(forBounds bounds: CGRect) -> CGRect {
        //set your bounds here
        return CGRect(origin: bounds.origin, size: CGSize(width:bounds.width, height:trackHeight))
    }

}
