//
//  DocBlogDetailsVC.swift
//  Brexa
//
//  Created by Jarvics on 07/11/17.
//  Copyright © 2017 Omninos_Solutions. All rights reserved.
//

import UIKit
import Alamofire

class DocBlogDetailsVC: UIViewController {

    @IBOutlet weak var activityView: UIView!
    @IBOutlet weak var detailBlogText: UITextView!
    @IBOutlet weak var descLbl: UILabel!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var coverImg: UIImageView!
    @IBOutlet weak var docImg: UIImageView!
    var name = String()
    var descText = String()
    var profile = String()
    var cover = String()
    var blogId = NSNumber()
    var tokenStr = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        print(blogId)
        self.activityView.isHidden = true
        if UserDefaults.standard.value(forKey: "authorizationToken") != nil
        {
            tokenStr = UserDefaults.standard.value(forKey: "authorizationToken") as! String
            print(tokenStr)
        }
        else{
            print("no token found")
        }
        nameLbl.text = name
        descLbl.text = descText
        
        if profile != "" {
            let url = URL(string: profile)!
            print(url)
            docImg.af_setImage(withURL: url)
        }
        else{
            docImg.image =  UIImage(named: "")
        }

        if cover != "" {
            let url = URL(string: cover)!
            print(url)
            coverImg.af_setImage(withURL: url)
        }
        else{
            coverImg.image =  UIImage(named: "")
        }
        BlogsDetail()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func BlogsDetail()
    {
        self.activityView.isHidden = false
        let headers = ["Content-Type": "application/json", "Authorization" :"Bearer \(tokenStr)"]
        Alamofire.request("http://vesnahealthsolutions.com/api/getBlog/\(blogId)", method: HTTPMethod.get,  encoding: URLEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            if response.result.value != nil
            {
                if (response.result.value as! NSDictionary).value(forKey: "status") as! Bool == true
                {
                    self.activityView.isHidden = true
                    print(response.result.value!)
                    let result = (response.result.value as! NSDictionary)
                    let blogData = (result as NSDictionary).value(forKey: "content") as! String
                    self.detailBlogText.text = blogData.htmlToString
                }
                else
                {
                    self.activityView.isHidden = true
                    let msg = (response.result.value as! NSDictionary).value(forKey: "message") as! String
                    Global().showSimpleAlert(Title: "Brexa", message: msg, inClass: self)           }
            }
            else
            {  //print(response.result.error!)
                self.activityView.isHidden = true
                let alert = UIAlertController(title: "Brexa", message: "Something went wrong please try again", preferredStyle: UIAlertControllerStyle.alert)
                
                alert.addAction(UIAlertAction(title:"Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated:true,completion: nil)
            }
        }
    }
    
    @IBAction func Backbtn(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func ShareBtn(_ sender: Any) {
        
    }
}
extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [NSAttributedString.DocumentReadingOptionKey.documentType:  NSAttributedString.DocumentType.html], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }}
