//
//  BreastExamVC.swift
//  Brexa
//
//  Created by Jarvics on 31/10/17.
//  Copyright © 2017 Omninos_Solutions. All rights reserved.
//

import UIKit
import AVFoundation

class BreastExamVC: UIViewController {

    @IBOutlet weak var playPauseV: UIView!
    var num = String()
    @IBOutlet weak var pauseBtn: UIButton!
    @IBOutlet weak var initialValue: UILabel!
    @IBOutlet weak var maximumValue: UILabel!
    @IBOutlet weak var sliderr: CustomSlide!
    @IBOutlet weak var activityView: UIView!
    @IBOutlet weak var playBtn: UIButton!
    @IBOutlet weak var audioImg: UIImageView!
   var audioPlayer:AVAudioPlayer!
    override func viewDidLoad() {
        super.viewDidLoad()
        setStatusBarBackgroundColor(color: UIColor(red: 187/255.0, green: 42/255.0, blue: 105/255.0, alpha: 1))
        num = "0"
        playPauseV.layer.cornerRadius = playPauseV.frame.size.width/2
        playPauseV.clipsToBounds = true

        activityView.isHidden = true
        audioImg.image = UIImage(named :"play")
        playBtn.isHidden = false
        pauseBtn.isHidden = true
        
        if let audioUrl = URL(string: "http://vesnahealthsolutions.com/video/english/pre_breast_exam_voiceover.mp3") {
            
            // then lets create your document folder url
            let documentsDirectoryURL =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
            
            // lets create your destination file url
            let destinationUrl = documentsDirectoryURL.appendingPathComponent(audioUrl.lastPathComponent)
            print(destinationUrl)
            
            // to check if it exists before downloading it
            if FileManager.default.fileExists(atPath: destinationUrl.path) {
                print("The file already exists at path")
                
            } else {
                
                URLSession.shared.downloadTask(with: audioUrl, completionHandler: { (location, response, error) -> Void in
                    guard let location = location, error == nil else { return }
                    do {
                        
                        try FileManager.default.moveItem(at: location, to: destinationUrl)
                        print("File moved to documents folder")
                    } catch let error as NSError {
                        print(error.localizedDescription)
                    }
                }).resume()
            }
        }
        
    }
   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func BackBtn(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
        
    }
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }
    
    func setStatusBarBackgroundColor(color: UIColor) {
        guard  let statusBar = (UIApplication.shared.value(forKey: "statusBarWindow") as AnyObject).value(forKey: "statusBar") as? UIView else {
            return
        }
        statusBar.backgroundColor = color
    }
    
    @IBAction func skipBtn(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "breastselfExamViewController") as! breastselfExamViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func playBtn(_ sender: Any) {
        audioImg.image = UIImage(named :"pause")
        playBtn.isHidden = true
        pauseBtn.isHidden = false
        
        if let audioUrl = URL(string: "http://vesnahealthsolutions.com/video/english/pre_breast_exam_voiceover.mp3") {
            
            // then lets create your document folder url
            let documentsDirectoryURL =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
            
            // lets create your destination file url
            let destinationUrl = documentsDirectoryURL.appendingPathComponent(audioUrl.lastPathComponent)
            
            //let url = Bundle.main.url(forResource: destinationUrl, withExtension: "mp3")!
            
            do {
                audioPlayer = try AVAudioPlayer(contentsOf: destinationUrl)
                guard let player = audioPlayer else { return }
                
                player.prepareToPlay()
                DispatchQueue.main.async {
                    self.sliderr.maximumValue = Float(self.audioPlayer.duration)
                    
                    let x =  self.sliderr.maximumValue
                    let y = Double(round(1*x)/1)
                    print()  // 1.236
                    let rate1 = String(y)
                    let twoDecimalPlaces = String(rate1)
                    print(rate1)
                    self.maximumValue.text = String(twoDecimalPlaces)
                    
                    self.sliderr.value = 0.0
                    self.initialValue.text = String(self.sliderr.value)
                }
                Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.upDateTime), userInfo: nil, repeats: true)
                 player.play()
                
            } catch let error {
                print(error.localizedDescription)
            }
        }
    }
    @objc func upDateTime(_ timer: Timer) {
        sliderr.value = Float(audioPlayer.currentTime)
        
        let x =   sliderr.value
        let y = Double(round(1*x)/1)
        print()  // 1.236
        let rate1 = String(y)
         self.initialValue.text = String(rate1)
        print("sliderr.value", sliderr.value)
    }
    @IBAction func sliderBtn(_ sender: Any) {
        audioPlayer.currentTime = TimeInterval(sliderr.value)
    }
    @IBAction func repeatBtn(_ sender: Any) {
        
        if let audioUrl = URL(string: "http://vesnahealthsolutions.com/video/english/pre_breast_exam_voiceover.mp3") {
            
            // then lets create your document folder url
            let documentsDirectoryURL =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
            
            // lets create your destination file url
            let destinationUrl = documentsDirectoryURL.appendingPathComponent(audioUrl.lastPathComponent)
            
            //let url = Bundle.main.url(forResource: destinationUrl, withExtension: "mp3")!
            
            do {
                audioPlayer = try AVAudioPlayer(contentsOf: destinationUrl)
                guard let player = audioPlayer else { return }
                
                player.prepareToPlay()
                DispatchQueue.main.async {
                    self.sliderr.maximumValue = Float(self.audioPlayer.duration)
                    
                    let x =  self.sliderr.maximumValue
                    let y = Double(round(1*x)/1)
                    print()  // 1.236
                    let rate1 = String(y)
                    
                    
                    let twoDecimalPlaces = String(rate1)
                    print(rate1)
                    self.maximumValue.text = String(twoDecimalPlaces)
                    
                    self.sliderr.value = 0.0
                    self.initialValue.text = String(self.sliderr.value)
                }
                Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.upDateTime), userInfo: nil, repeats: true)
                player.play()
                
            } catch let error {
                print(error.localizedDescription)
            }
        }
        }
    
    @IBAction func pauseBtn(_ sender: Any) {
        
    if  audioPlayer.isPlaying{
    audioPlayer.pause()
    audioImg.image = UIImage(named :"play")
    playBtn.isHidden = true
    pauseBtn.isHidden = false
    }
    else{
        
    audioPlayer.play()
    audioImg.image = UIImage(named :"pause")
    playBtn.isHidden = true
    pauseBtn.isHidden = false
    }
    }
    
    @IBAction func startSelfExam(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "breastselfExamViewController") as! breastselfExamViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
