//
//  MtDocDetailVC.swift
//  Brexa
//
//  Created by Jarvics on 01/11/17.
//  Copyright © 2017 Omninos_Solutions. All rights reserved.
//

import UIKit

class MtDocDetailVC: UIViewController,UIScrollViewDelegate {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var aboutLbl: UILabel!
    @IBOutlet weak var view3: UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var view1: UIView!
    
    var rate = Double()
    var address = String()
    var about = String()
    var name = String()
    var profile = String()
    var education = String()
    var speciality = String()
    var Phone = String()

    @IBOutlet weak var educationLbl1: UILabel!
    @IBOutlet weak var editView: UIView!
    @IBOutlet weak var specialityLbl: UILabel!
    @IBOutlet weak var picture: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var view4: UITextView!
    @IBOutlet weak var addLbl: UILabel!
    @IBOutlet weak var ratingLbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        editView.layer.cornerRadius = editView.frame.size.width/2
        editView.clipsToBounds = true
        scrollView.isScrollEnabled=true;
        scrollView.contentSize = CGSize(width: 0 , height: self.contentView.frame.size.height)
        scrollView.delegate = self
        view1.layer.shadowColor = UIColor.lightGray.cgColor
        view1.layer.shadowOffset = CGSize(width:0,height: 1.5)
        view1.layer.shadowRadius = 2.0
        view1.layer.shadowOpacity = 1.0
        
        view2.layer.shadowColor = UIColor.lightGray.cgColor
        view2.layer.shadowOffset = CGSize(width:0,height: 1.5)
        view2.layer.shadowRadius = 2.0
        view2.layer.shadowOpacity = 1.0
        
        view3.layer.shadowColor = UIColor.lightGray.cgColor
        view3.layer.shadowOffset = CGSize(width:0,height: 1.5)
        view3.layer.shadowRadius = 2.0
        view3.layer.shadowOpacity = 1.0
        
        view4.layer.shadowColor = UIColor.lightGray.cgColor
        view4.layer.shadowOffset = CGSize(width:0,height: 1.5)
        view4.layer.shadowRadius = 2.0
        view4.layer.shadowOpacity = 1.0
        
        if profile != "" {
            let url = URL(string: profile)!
            print(url)
            picture.af_setImage(withURL: url)
            picture.autoresizingMask = [.flexibleWidth, .flexibleHeight, .flexibleBottomMargin, .flexibleRightMargin, .flexibleLeftMargin, .flexibleTopMargin]
            picture.contentMode = .scaleAspectFill // OR .scaleAspectFill
            picture.clipsToBounds = true
        }
        else{
            picture.image =  UIImage(named: "Mamogram lab")
        }
        
        nameLbl.text = name
        addLbl.text = address
        educationLbl1.text = education
        specialityLbl.text = speciality
        addLbl.text = address

        
        if rate == 0.0
        {
            view1.isHidden = true
//            view1.frame.origin.y = self.pictureView.frame.size.height + 100
        }
        else
        {
            let x = rate
            let y = Double(round(10*x)/10)
            print()  // 1.236
            let rate1 = String(y)
            ratingLbl.text = "\(rate1)/5.0"
        }
        
        if about == ""
        {
            view4.isHidden = true
            aboutLbl.isHidden = true
            //            view1.frame.origin.y = self.pictureView.frame.size.height + 100
        }
        else
        {
            view4.text = about
            
        }
    }

    @IBAction func editBtn(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "HostptdetailRatingVC") as! HostptdetailRatingVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func backBtn(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func reviewbtn(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ReviewVC") as! ReviewVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func callBtn(_ sender: Any) {
        
        if let phoneCallURL:URL = URL(string: "tel:\(self.Phone)") {
            let application:UIApplication = UIApplication.shared
            if (application.canOpenURL(phoneCallURL)) {
                let alertController = UIAlertController(title: "Hashy", message: "Are you sure you want to call \n\(self.Phone)?", preferredStyle: .alert)
                let yesPressed = UIAlertAction(title: "Yes", style: .default, handler: { (action) in
                    UIApplication.shared.open(phoneCallURL)
                })
                let noPressed = UIAlertAction(title: "No", style: .default, handler: { (action) in
                    
                })
                alertController.addAction(yesPressed)
                alertController.addAction(noPressed)
                present(alertController, animated: true, completion: nil)
            }
        }
    }
    
}
