//
//  MeetSpecialistVC.swift
//  Brexa
//
//  Created by Jarvics on 01/11/17.
//  Copyright © 2017 Omninos_Solutions. All rights reserved.
//

import UIKit
import CoreLocation

class MeetSpecialistVC: UIViewController,UITableViewDataSource,UITableViewDelegate,UIScrollViewDelegate,CLLocationManagerDelegate {

    @IBOutlet weak var activityView: UIView!
    @IBOutlet var MostRatedView: UIView!
    @IBOutlet var NearView: UIView!
    @IBOutlet var NeartableView: UITableView!
    var manager = CLLocationManager()
    var currentLocation = CLLocationCoordinate2D()
    var NearData1 = NSArray()
    var mostRateData1 = NSArray()
    var num = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor(red: 187/255.0, green: 42/255.0, blue: 105/255.0, alpha: 1)
         self.activityView.isHidden = true
        manager.delegate = self
    manager.requestWhenInUseAuthorization()
        manager.startUpdatingLocation()
        NeartableView.rowHeight = UITableViewAutomaticDimension
        NeartableView.estimatedRowHeight = 300
        NearView.isHidden = false
        MostRatedView.isHidden = true
        num = "1"
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeRight.direction = UISwipeGestureRecognizerDirection.right
        self.view.addGestureRecognizer(swipeRight)
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeLeft.direction = UISwipeGestureRecognizerDirection.left
        self.view.addGestureRecognizer(swipeLeft)
    }
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }
    
    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.right:
                if  NearView.isHidden == false
                {
                    
                }
                else
                {
                num = "1"
                NearView.isHidden = false
                MostRatedView.isHidden = true
                  GetNearMammo()
                }

            case UISwipeGestureRecognizerDirection.left:
                
                if  MostRatedView.isHidden == false
                {
                    
                }
                else{
                num = "2"
                NearView.isHidden = true
                MostRatedView.isHidden = false
                GetMostRated()
                }
                
            default:
                break
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]){
        currentLocation = (manager.location?.coordinate)!
        print(currentLocation)
        manager.stopUpdatingLocation()
        GetNearMammo()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
           if num == "1"
           {
            return NearData1.count
           }
          else
          { return mostRateData1.count
          }
        }

     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = NeartableView.dequeueReusableCell(withIdentifier: "MeetTableViewCell")as! MeetTableViewCell
        if num == "1"
        {
            cell.mainView.layer.shadowColor = UIColor.lightGray.cgColor
            cell.mainView.layer.shadowOffset = CGSize(width:0,height: 1.5)
            cell.mainView.layer.shadowRadius = 2.0
            cell.mainView.layer.shadowOpacity
                = 1.0
            cell.mammoName.text =  (NearData1.value(forKey: "name")as! NSArray)[indexPath.row] as? String
             cell.qualification.text =  (NearData1.value(forKey: "qualification")as! NSArray)[indexPath.row] as? String
             cell.specializationLbl.text =  (NearData1.value(forKey: "specialty")as! NSArray)[indexPath.row] as? String
            
            let blogprofile2 = (NearData1.value(forKey: "profile_pic")as! NSArray)[indexPath.row] as? String
            
            if ((NearData1.value(forKey: "rating")as! NSArray)[indexPath.row] is NSNull)
            {    cell.star1.image = UIImage(named: "star")
               cell.star2.image = UIImage(named: "star")
                cell.star3.image = UIImage(named: "star")
                cell.star4.image = UIImage(named: "star")
                cell.star5.image = UIImage(named: "star")
            }
            else{
                let ratingNum = (NearData1.value(forKey: "rating")as! NSArray)[indexPath.row] as? NSNumber
                
                let rateDouble = Double(ratingNum!)
                let ratenew = round(rateDouble)
                
                if ratenew == 0{
                    cell.star1.image = UIImage(named: "star")
                    cell.star2.image = UIImage(named: "star")
                    cell.star3.image = UIImage(named: "star")
                    cell.star4.image = UIImage(named: "star")
                    cell.star5.image = UIImage(named: "star")
                }
                else if ratenew == 1{
                    cell.star1.image = UIImage(named: "ratestar")
                    cell.star2.image = UIImage(named: "star")
                    cell.star3.image = UIImage(named: "star")
                    cell.star4.image = UIImage(named: "star")
                    cell.star5.image = UIImage(named: "star")
                }
                else if ratenew == 2{
                    cell.star1.image = UIImage(named: "ratestar")
                    cell.star2.image = UIImage(named: "ratestar")
                    cell.star3.image = UIImage(named: "star")
                    cell.star4.image = UIImage(named:"star")
                    cell.star5.image = UIImage(named: "star")
                    
                }
                else if ratenew == 3{
                    cell.star1.image = UIImage(named: "ratestar")
                    cell.star2.image = UIImage(named: "ratestar")
                    cell.star3.image = UIImage(named: "ratestar")
                    cell.star4.image = UIImage(named: "star")
                    cell.star5.image = UIImage(named: "star")
                    
                }
                    
                else if ratenew == 4{
                    cell.star1.image = UIImage(named: "ratestar")
                    cell.star2.image = UIImage(named: "ratestar")
                    cell.star3.image = UIImage(named: "ratestar")
                    cell.star4.image = UIImage(named: "ratestar")
                    cell.star5.image = UIImage(named: "star")
                }
                    
                else if ratenew == 5{
                    cell.star1.image = UIImage(named: "ratestar")
                    cell.star2.image = UIImage(named: "ratestar")
                    cell.star3.image = UIImage(named: "ratestar")
                    cell.star4.image = UIImage(named: "ratestar")
                    cell.star5.image = UIImage(named: "ratestar")
                }
            }
            if blogprofile2 != "" {
                let url = URL(string: blogprofile2!)!
                print(url)
                cell.mammoImg.af_setImage(withURL: url)
            }
            else{
                cell.mammoImg.image =  UIImage(named: "Mamogram lab")
            }
            
            return cell
        }
        else
        { cell.mammoName.text =  (mostRateData1.value(forKey: "name")as! NSArray)[indexPath.row] as? String
            
            cell.mainView.layer.shadowColor = UIColor.lightGray.cgColor
            cell.mainView.layer.shadowOffset = CGSize(width:0,height: 1.5)
            cell.mainView.layer.shadowRadius = 2.0
            cell.mainView.layer.shadowOpacity
                = 1.0
            cell.qualification.text =  (mostRateData1.value(forKey: "qualification")as! NSArray)[indexPath.row] as? String
            cell.specializationLbl.text =  (mostRateData1.value(forKey: "specialty")as! NSArray)[indexPath.row] as? String
            
            let blogprofile2 = (mostRateData1.value(forKey: "profile_pic")as! NSArray)[indexPath.row] as? String
            
            if ((mostRateData1.value(forKey: "rating")as! NSArray)[indexPath.row] is NSNull)
            {cell.star1.image = UIImage(named: "star")
                cell.star2.image = UIImage(named: "star")
                cell.star3.image = UIImage(named: "star")
                cell.star4.image = UIImage(named: "star")
                cell.star5.image = UIImage(named: "star")
            }
            else{
                let ratingNum = (mostRateData1.value(forKey: "rating")as! NSArray)[indexPath.row] as? NSNumber
                
                let rateDouble = Double(ratingNum!)
                let ratenew = round(rateDouble)
                
                if ratenew == 0{
                    cell.star1.image = UIImage(named: "star")
                    cell.star2.image = UIImage(named: "star")
                    cell.star3.image = UIImage(named: "star")
                    cell.star4.image = UIImage(named: "star")
                    cell.star5.image = UIImage(named: "star")
                }
                else if ratenew == 1{
                    cell.star1.image = UIImage(named: "ratestar")
                    cell.star2.image = UIImage(named: "star")
                    cell.star3.image = UIImage(named: "star")
                    cell.star4.image = UIImage(named: "star")
                    cell.star5.image = UIImage(named: "star")
                }
                else if ratenew == 2{
                    cell.star1.image = UIImage(named: "ratestar")
                    cell.star2.image = UIImage(named: "ratestar")
                    cell.star3.image = UIImage(named: "star")
                    cell.star4.image = UIImage(named:"star")
                    cell.star5.image = UIImage(named: "star")
                    
                }
                else if ratenew == 3{
                    cell.star1.image = UIImage(named: "ratestar")
                    cell.star2.image = UIImage(named: "ratestar")
                    cell.star3.image = UIImage(named: "ratestar")
                    cell.star4.image = UIImage(named: "star")
                    cell.star5.image = UIImage(named: "star")
                    
                }
                    
                else if ratenew == 4{
                    cell.star1.image = UIImage(named: "ratestar")
                    cell.star2.image = UIImage(named: "ratestar")
                    cell.star3.image = UIImage(named: "ratestar")
                    cell.star4.image = UIImage(named: "ratestar")
                    cell.star5.image = UIImage(named: "star")
                }
                    
                else if ratenew == 5{
                    cell.star1.image = UIImage(named: "ratestar")
                    cell.star2.image = UIImage(named: "ratestar")
                    cell.star3.image = UIImage(named: "ratestar")
                    cell.star4.image = UIImage(named: "ratestar")
                    cell.star5.image = UIImage(named: "ratestar")
                }
            }
            if blogprofile2 != "" {
                let url = URL(string: blogprofile2!)!
                print(url)
                cell.mammoImg.af_setImage(withURL: url)
            }
            else{
                cell.mammoImg.image =  UIImage(named: "Mamogram lab")
            }
            return cell
        }
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if num == "1"{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "MtDocDetailVC") as! MtDocDetailVC
            if ((NearData1.value(forKey: "rating")as! NSArray)[indexPath.row] is NSNull)
            {
            }
            else{
                let ratingNum = (NearData1.value(forKey: "rating")as! NSArray)[indexPath.row] as? NSNumber
                let rateDouble = Double(ratingNum!)
                
                vc.rate = rateDouble
            }
            if ((NearData1.value(forKey: "description")as! NSArray)[indexPath.row] is NSNull)
            {
                vc.about = ""
            }
            else
            {
                vc.about = ((NearData1.value(forKey: "description")as! NSArray)[indexPath.row] as? String)!
            }
            if ((NearData1.value(forKey: "place")as! NSArray)[indexPath.row] is NSNull)
            {
                vc.address = ""
            }
            else
            {
                vc.address = ((NearData1.value(forKey: "place")as! NSArray)[indexPath.row] as? String)!
            }
            if ((NearData1.value(forKey: "name")as! NSArray)[indexPath.row] is NSNull)
            {
                vc.name = ""
            }
            else
            {
                vc.name = ((NearData1.value(forKey: "name")as! NSArray)[indexPath.row] as? String)!
            }
            
            if ((NearData1.value(forKey: "phone")as! NSArray)[indexPath.row] is NSNull)
            {
                vc.Phone = ""
            }
            else
            {
                vc.Phone = ((NearData1.value(forKey: "phone")as! NSArray)[indexPath.row] as? String)!
            }
            if ((NearData1.value(forKey: "profile_pic")as! NSArray)[indexPath.row] is NSNull)
            {
            }
            else
            {
                vc.profile = ((NearData1.value(forKey: "profile_pic")as! NSArray)[indexPath.row] as? String)!
            }
            if ((NearData1.value(forKey: "qualification")as! NSArray)[indexPath.row] is NSNull)
            {vc.education = ""
            }
            else
            {
                vc.education = ((NearData1.value(forKey: "qualification")as! NSArray)[indexPath.row] as? String)!
            }
            if ((NearData1.value(forKey: "specialty")as! NSArray)[indexPath.row] is NSNull)
            {vc.speciality = ""
            }
            else
            {
                vc.speciality = ((NearData1.value(forKey: "specialty")as! NSArray)[indexPath.row] as? String)!
            }
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        else
        {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "MtDocDetailVC") as! MtDocDetailVC
            if ((mostRateData1.value(forKey: "rating")as! NSArray)[indexPath.row] is NSNull)
            {
            }
            else{
                let ratingNum = (mostRateData1.value(forKey: "rating")as! NSArray)[indexPath.row] as? NSNumber
                let rateDouble = Double(ratingNum!)
                
                vc.rate = rateDouble
            }
            
            if ((mostRateData1.value(forKey: "phone")as! NSArray)[indexPath.row] is NSNull)
            {
                vc.Phone = ""
            }
            else
            {
                vc.Phone = ((mostRateData1.value(forKey: "phone")as! NSArray)[indexPath.row] as? String)!
            }
            
            if ((mostRateData1.value(forKey: "qualification")as! NSArray)[indexPath.row] is NSNull)
            {
                vc.education = ""
            }
            else
            {
                vc.education = ((mostRateData1.value(forKey: "qualification")as! NSArray)[indexPath.row] as? String)!
            }
            if ((mostRateData1.value(forKey: "specialty")as! NSArray)[indexPath.row] is NSNull)
            {vc.speciality = ""
            }
            else
            {
                vc.speciality = ((mostRateData1.value(forKey: "specialty")as! NSArray)[indexPath.row] as? String)!
            }
            if ((mostRateData1.value(forKey: "description")as! NSArray)[indexPath.row] is NSNull)
            {
                vc.about = ""
            }
            else
            {
                vc.about = ((mostRateData1.value(forKey: "description")as! NSArray)[indexPath.row] as? String)!
            }
            if ((mostRateData1.value(forKey: "place")as! NSArray)[indexPath.row] is NSNull)
            {
                vc.address = ""
            }
            else
            {
                vc.address = ((mostRateData1.value(forKey: "place")as! NSArray)[indexPath.row] as? String)!
            }
            if ((mostRateData1.value(forKey: "name")as! NSArray)[indexPath.row] is NSNull)
            {
                vc.name = ""
            }
            else
            {
                vc.name = ((mostRateData1.value(forKey: "name")as! NSArray)[indexPath.row] as? String)!
            }
            if ((mostRateData1.value(forKey: "profile_pic")as! NSArray)[indexPath.row] is NSNull)
            {
            }
            else
            {
                vc.profile = ((mostRateData1.value(forKey: "profile_pic")as! NSArray)[indexPath.row] as? String)!
            }
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    @IBAction func Mostratbtn(_ sender: Any) {
        num = "2"
        GetMostRated()
        NearView.isHidden = true
        MostRatedView.isHidden = false
    }
    @IBAction func NearbyBtn(_ sender: Any) {
        num = "1"
        GetNearMammo()
        NearView.isHidden = false
        MostRatedView.isHidden = true
    }
    
    @IBAction func BAckBtn(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }
   func GetNearMammo()
    {
        let lat = String(currentLocation.latitude)
        let long = String(currentLocation.longitude)
        print(lat)
         print(lat)
        activityView.isHidden = false
        let userinfo:NSDictionary = [:]
        
        Global().alamofire_withGetMethod("doc/doctor/\(lat)/\(long)", parameters: userinfo, completion: { (result) in
            print("Product data",result)
            let msg = (result as? NSDictionary)?.value(forKey: "message") as! String?
            if (result as! NSDictionary).value(forKey: "status") as? Bool == true
            {
                self.activityView.isHidden = true
                self.NearData1 =  (result as! NSDictionary).value(forKey: "doc") as! NSArray
                print("get mammogram data",self.NearData1)
                self.NeartableView.reloadData()
            }
            else{
                self.activityView.isHidden = true
                Global().showSimpleAlert(Title: "Hashy", message: msg!, inClass: self)
            }
        })
        {(failure) in
            
            self.activityView.isHidden = true
            let alert = UIAlertController(title: "Invalid", message: "No internet connection", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title:"Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated:true,completion: nil)
        }
    }
    
    func GetMostRated()
    {
        activityView.isHidden = false
        let userinfo:NSDictionary = [:]
        Global().alamofire_withGetMethod("doc/orderByRating/doctor", parameters: userinfo, completion: { (result) in
            print("Product data",result)
            let msg = (result as? NSDictionary)?.value(forKey: "message") as! String?
            if (result as! NSDictionary).value(forKey: "status") as? Bool == true
            {
                self.activityView.isHidden = true
                self.mostRateData1 =  (result as! NSDictionary).value(forKey: "doc") as! NSArray
                print("get mammogram data",self.mostRateData1)
                self.NeartableView.reloadData()
            }
            else{
                self.activityView.isHidden = true
                Global().showSimpleAlert(Title: "Hashy", message: msg!, inClass: self)
            }
        })
        {(failure) in
        
            self.activityView.isHidden = true
            let alert = UIAlertController(title: "Invalid", message: "No internet connection", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title:"Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated:true,completion: nil)
        }
    }
}
