//
//  BlogCollectionViewCell.swift
//  Brexa
//
//  Created by Jarvics on 30/10/17.
//  Copyright © 2017 Omninos_Solutions. All rights reserved.
//

import UIKit

class BlogCollectionViewCell: UICollectionViewCell {
    @IBOutlet var BlogInvire: UIView!
    
    @IBOutlet weak var blogsDataView: UIView!
    @IBOutlet weak var ImageView: UIView!
    @IBOutlet weak var blogName: UILabel!
    @IBOutlet weak var blogProfile: UIImageView!
    
    @IBOutlet weak var blogText: UITextView!
    @IBOutlet weak var blogCover: UIImageView!
    
    @IBOutlet weak var blogDesc: UILabel!
}
