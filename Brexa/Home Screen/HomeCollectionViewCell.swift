//
//  HomeCollectionViewCell.swift
//  Brexa
//
//  Created by Jarvics on 30/10/17.
//  Copyright © 2017 Omninos_Solutions. All rights reserved.
//

import UIKit

class HomeCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var bannerImg: UIImageView!
}
