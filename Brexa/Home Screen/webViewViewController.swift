//
//  webViewViewController.swift
//  Aappii
//
//  Created by Jarvics  on 19/07/17.
//  Copyright © 2017 Omninos_Solutions. All rights reserved.
//

import UIKit

class webViewViewController: UIViewController {

    @IBOutlet weak var webView: UIWebView!
    
    var link: URL!
   // var link2: URL!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //print(link)
       // print(link2)
        webView.scrollView.bounces = false
        
        webView.loadRequest(NSURLRequest(url:link as URL) as URLRequest)
//       webView.loadRequest(NSURLRequest(url:link2 as URL) as URLRequest)

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func back(_ sender: Any) {
        
         _ = self.navigationController?.popViewController(animated: true)
    }
    
}
