//
//  HomeVC.swift
//  Brexa
//
//  Created by Jarvics on 30/10/17.
//  Copyright © 2017 Omninos_Solutions. All rights reserved.
//

import UIKit
import Gemini
import Alamofire
class HomeVC: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UIScrollViewDelegate,UITableViewDelegate,UITableViewDataSource {

   
    @IBOutlet weak var dataView: UIView!
    @IBOutlet weak var sidemenuSignIn: UIButton!
    @IBOutlet weak var alertView: UIView!
    var a = Int()
      var x = 1
     var begin = false
    var bannerArray = NSArray()
    var profile = String()
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var profileImg: UIImageView!
    @IBOutlet weak var activityView: UIView!
    @IBOutlet var frstView: UIView!
    @IBOutlet var CateView: UIView!
    @IBOutlet var PageController: UIPageControl!
    @IBOutlet var BlackBtn: UIButton!
    @IBOutlet var MenuView: UIView!
    @IBOutlet var MenuTableView: UITableView!
    @IBOutlet var LAstView: UIView!
    @IBOutlet var AllView: UIView!
    @IBOutlet var ScrollView: UIScrollView!
    @IBOutlet var HomeCollectionView: UICollectionView!
    @IBOutlet var BlogCollectionView: UICollectionView!
    var blogsArray = NSArray()
   var nextItem = Int()
    var tokenStr = String()
    var Imagearr : NSArray = ["doc","englishwom","muslimwom"]
    var tiltearr  = [["Medical reports","Vouchers","View Profile","Set/Edit Pin","Choose your language","Disclaimer","Share this application","Rate us","Exit"],["Diagnostic tests","Who's at high risk"]]
    var Picarr = [["ic_read","ic_vouchers","ic_profile","ic_pin","ic_language","ic_info","ic_share_black","ic_rate_star","ic_logout"],["ic_read","ic_risk"]]
    var titlearr2 = ["Diagnostic tests","Who's at high risk"]
    let
    section1 = ["","About Breast Cancer"]
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setStatusBarBackgroundColor(color: UIColor(red: 187/255.0, green: 42/255.0, blue: 105/255.0, alpha: 1))
        alertView.isHidden = true
         BlackBtn.isHidden = true
         activityView.isHidden = true
        frstView.layer.shadowColor = UIColor.lightGray.cgColor
        frstView.layer.shadowOffset = CGSize(width:0,height: 1.5)
        frstView.layer.shadowRadius = 2.0
        frstView.layer.shadowOpacity = 1.0
        CateView.layer.shadowColor = UIColor.lightGray.cgColor
        CateView.layer.shadowOffset = CGSize(width:0,height: 1.5)
        CateView.layer.shadowRadius = 0
        CateView.layer.shadowOpacity = 1.0
       // startTimer()
        
        ScrollView.isScrollEnabled=true;
        ScrollView.contentSize = CGSize(width: 0 , height: self.dataView.frame.size.height)
        if UserDefaults.standard.value(forKey: "authorizationToken") != nil
        {
            tokenStr = UserDefaults.standard.value(forKey: "authorizationToken") as! String
            nameLbl.isHidden = false
            sidemenuSignIn.isHidden = true
            print(tokenStr)
            getProfileApi()
            BannerApi()
            BlogsApi()
        }
        else{
            nameLbl.isHidden = true
            sidemenuSignIn.isHidden = false
        }
        
         self.PageController.numberOfPages = self.bannerArray.count
    }
    func setStatusBarBackgroundColor(color: UIColor) {
        guard  let statusBar = (UIApplication.shared.value(forKey: "statusBarWindow") as AnyObject).value(forKey: "statusBar") as? UIView else {
            return
        }
        statusBar.backgroundColor = color
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }
    
    @objc func autoScroll() {
        if self.x < self.bannerArray.count {
            let indexPath = IndexPath(item: x, section: 0)
            self.HomeCollectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
            self.x = self.x + 1
            
            let pageNumber = round(HomeCollectionView.contentOffset.x / HomeCollectionView.frame.size.width)
            
            a = Int(pageNumber)
            PageController.currentPage = Int(pageNumber)
        } else {
            self.x = 0
            self.HomeCollectionView.scrollToItem(at: IndexPath(item: 0, section: 0), at: .centeredHorizontally, animated: true)
            let pageNumber = round(HomeCollectionView.contentOffset.x / HomeCollectionView.frame.size.width)
            a = Int(pageNumber)
            PageController.currentPage = Int(pageNumber)
        }
    }
    func setTimer() {
        let _ = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(HomeVC.autoScroll), userInfo: nil, repeats: true)
    }
    
    func changePage(sender: AnyObject) -> () {
                let x = CGFloat(PageController.currentPage) * HomeCollectionView.frame.size.width
                HomeCollectionView.setContentOffset(CGPoint(x:x, y:0), animated: true)
            }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
            let pageNumber = round(HomeCollectionView.contentOffset.x / HomeCollectionView.frame.size.width)
            
            a = Int(pageNumber)
            PageController.currentPage = Int(pageNumber)
            
        }
   
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if UserDefaults.standard.value(forKey: "authorizationToken") != nil
        {
            getProfileApi()
        }
        else{
//            alertView.isHidden = false
//            BlackBtn.isHidden = false
        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == HomeCollectionView{
            return bannerArray.count
        }else{
        return blogsArray.count
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == HomeCollectionView{
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCollectionViewCell", for: indexPath)as! HomeCollectionViewCell
            
            let profile = (bannerArray.value(forKey: "image")as! NSArray)[indexPath.row] as? String
            
            if profile != "" {
                let url = URL(string: profile!)!
                print(url)
                cell.bannerImg.af_setImage(withURL: url)
//                cell.bannerImg.autoresizingMask = [.flexibleWidth, .flexibleHeight, .flexibleBottomMargin, .flexibleRightMargin, .flexibleLeftMargin, .flexibleTopMargin]
//                cell.bannerImg.contentMode = .scaleAspectFit // OR .scaleAspectFill
//                cell.bannerImg.clipsToBounds = true
            }
            else{
                cell.bannerImg.image =  UIImage(named: "")
            }
            
        return cell
        }else{
            let cell2 = BlogCollectionView.dequeueReusableCell(withReuseIdentifier: "BlogCollectionViewCell", for: indexPath)as! BlogCollectionViewCell
            
            cell2.blogsDataView.layer.shadowColor = UIColor.lightGray.cgColor
            cell2.blogsDataView.layer.shadowOffset = CGSize(width:0,height: 1.5)
            cell2.blogsDataView.layer.shadowRadius = 2.0
            cell2.blogsDataView.layer.shadowOpacity
                = 1.0
            
            cell2.ImageView.layer.cornerRadius = cell2.ImageView.frame.size.width/2
            cell2.ImageView.clipsToBounds = true
            
            cell2.ImageView.layer.borderWidth = 3.0
            cell2.ImageView.layer.borderColor = UIColor.white.cgColor
            cell2.BlogInvire.layer.shadowColor = UIColor.lightGray.cgColor
            cell2.BlogInvire.layer.shadowOffset = CGSize(width:0,height: 1.5)
            cell2.BlogInvire.layer.shadowRadius = 2.0
            cell2.BlogInvire.layer.shadowOpacity = 1.0
            
            cell2.blogName.text = (blogsArray.value(forKey: "name")as! NSArray)[indexPath.row] as? String
             cell2.blogText.text = (blogsArray.value(forKey: "content")as! NSArray)[indexPath.row] as? String
             cell2.blogDesc.text = (blogsArray.value(forKey: "title")as! NSArray)[indexPath.row] as? String
            
            let blogprofile1 = (blogsArray.value(forKey: "profile_pic")as! NSArray)[indexPath.row] as? String
            
            if blogprofile1 != "" {
                let url = URL(string: blogprofile1!)!
                print(url)
                cell2.blogProfile.af_setImage(withURL: url)
            }
            else{
                cell2.blogProfile.image =  UIImage(named: "maledoc")
            }
            
            let blogprofile2 = (blogsArray.value(forKey: "image")as! NSArray)[indexPath.row] as? String
            
            if blogprofile2 != "" {
                let url = URL(string: blogprofile2!)!
                print(url)
                cell2.blogCover.af_setImage(withURL: url)
            }
            else{
                cell2.blogCover.image =  UIImage(named: "")
            }
        
            return cell2
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == HomeCollectionView
        {
         let linkType = (bannerArray.value(forKey: "link_type")as! NSArray)[indexPath.row] as? String
        if linkType == "web"
        {
            let link  = (bannerArray.value(forKey: "link")as! NSArray)[indexPath.row] as? String
            
            if let url = URL(string: link!)
            {
            UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
            }
        }
        else if linkType == "diagnostictest"
        {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "DiagnosticTestVC") as! DiagnosticTestVC
                self.navigationController?.pushViewController(vc, animated: true)
        }
        else if linkType == "registration"
        {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignupVC") as! SignupVC
                self.navigationController?.pushViewController(vc, animated: true)
        }
        else if linkType == "voucher"
        {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "VoucherVC") as! VoucherVC
                self.navigationController?.pushViewController(vc, animated: true)
        }
        }
        else
        {
             let blogprofile1 = (blogsArray.value(forKey: "profile_pic")as! NSArray)[indexPath.row] as? String
             let blogprofile2 = (blogsArray.value(forKey: "image")as! NSArray)[indexPath.row] as? String
             let blogTitle = (blogsArray.value(forKey: "title")as! NSArray)[indexPath.row] as? String
            let blogName = (blogsArray.value(forKey: "name")as! NSArray)[indexPath.row] as? String
             let B_id = (blogsArray.value(forKey: "id")as! NSArray)[indexPath.row] as? NSNumber
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "DocBlogDetailsVC") as! DocBlogDetailsVC
            
            vc.name = blogName!
            vc.profile = blogprofile1!
            vc.cover = blogprofile2!
            vc.descText = blogTitle!
            vc.blogId = B_id!
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == HomeCollectionView{
        let cellsize = CGSize( width:self.HomeCollectionView.frame.size.width , height:self.HomeCollectionView.frame.size.height)
            return cellsize
        }
        else{
            let cellsize2 = CGSize(width:self.BlogCollectionView.frame.size.width/2 ,height:self.BlogCollectionView.frame.size.height/2 + 70)
            return cellsize2
        }
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return self.section1[section]
    }

    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections

        return self.section1.count

    }
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return section1.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (tiltearr[section] as AnyObject).count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = MenuTableView.dequeueReusableCell(withIdentifier: "HomeTableViewCell")as! HomeTableViewCell
        cell.titleimage.image = UIImage(named: Picarr[indexPath.section][indexPath.row])
        cell.Titlelbl.text = self.tiltearr[indexPath.section][indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0{
            if indexPath.row == 0{
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "medicalReportsViewController") as! medicalReportsViewController
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else if indexPath.row == 1{
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "VoucherVC") as! VoucherVC
                self.navigationController?.pushViewController(vc, animated: true)
            }
           else if indexPath.row == 2{
                
                if UserDefaults.standard.value(forKey: "authorizationToken") != nil
                {
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                else{
                    UIView.animate(withDuration: 0.4, animations: {
                        self.MenuView.frame.origin.x = -self.MenuView.frame.size.width - 10
                    })
                    alertView.isHidden = false
                    BlackBtn.isHidden = false
                }
            }
            else if indexPath.row == 3{
                
                if UserDefaults.standard.value(forKey: "authorizationToken") != nil
                {
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "SetPinVC") as! SetPinVC
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                else{
                    UIView.animate(withDuration: 0.4, animations: {
                        self.MenuView.frame.origin.x = -self.MenuView.frame.size.width - 10
                    })
                    alertView.isHidden = false
                    BlackBtn.isHidden = false
                }
               
            }
            else if indexPath.row == 4{
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "LanguageVC") as! LanguageVC
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else if indexPath.row == 5{
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "DisclaimerVC") as! DisclaimerVC
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else if indexPath.row == 6{
                let activityVc = UIActivityViewController(activityItems: ["https://developer.apple.com/"], applicationActivities: nil)
                activityVc.popoverPresentationController?.sourceView=self.view;

                self.present(activityVc,animated: true,completion: nil)
            }
            else if indexPath.row == 7{

            }else{
                
                if UserDefaults.standard.value(forKey: "authorizationToken") != nil
                {
                     logOutApi()
                }
                else{
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "PlayintroVC") as! PlayintroVC
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }else{
            if indexPath.row == 0{
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "DiagnosticTestVC") as! DiagnosticTestVC
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else if indexPath.row == 1{
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "WomenRiskVC") as! WomenRiskVC
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        (view as! UITableViewHeaderFooterView).backgroundView?.backgroundColor = UIColor.white.withAlphaComponent(0.4)
        if let header = view as? UITableViewHeaderFooterView {
            header.textLabel!.font = UIFont.systemFont(ofSize: 13.0)
       }
    }
    @IBAction func MenuBtn(_ sender: Any) {
        BlackBtn.isHidden = false
        MenuView.isHidden = false
        UIView.animate(withDuration: 0.4, animations: {
            self.MenuView.frame.origin.x = 0
        })
    }
    @IBAction func BlackBtn(_ sender: Any) {
        BlackBtn.isHidden = true
        alertView.isHidden = true
        UIView.animate(withDuration: 0.4, animations: {
            self.MenuView.frame.origin.x = -self.MenuView.frame.size.width - 10
        })
    }
    
    @IBAction func alertsignInBtn(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignInVC") as! SignInVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func BreastExamBtn(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "BreastExamVC") as! BreastExamVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func BookMammoBtn(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "BookMammogramVC") as! BookMammogramVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func MeetSpecialBtn(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MeetSpecialistVC") as! MeetSpecialistVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func CheckBrestcancerbtn(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CheckBreastCancerVC") as! CheckBreastCancerVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func sideMenusignInBtn(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignInVC") as! SignInVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func ProfileBtn(_ sender: Any) {
        if UserDefaults.standard.value(forKey: "authorizationToken") != nil
        {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else{
            UIView.animate(withDuration: 0.4, animations: {
                self.MenuView.frame.origin.x = -self.MenuView.frame.size.width - 10
            })
            alertView.isHidden = false
            BlackBtn.isHidden = false
        }
    }
    
    func getProfileApi()
    {
        self.activityView.isHidden = false
        let headers = ["Content-Type": "application/json", "Authorization" :"Bearer \(tokenStr)"]
        
        Alamofire.request("http://vesnahealthsolutions.com/api/profile", method: HTTPMethod.get,  encoding: URLEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            if response.result.value != nil
            {
                if (response.result.value as! NSDictionary).value(forKey: "status") as! Bool == true
                {
                    self.activityView.isHidden = true
                    print(response.result.value!)
                    let result = (response.result.value as! NSDictionary).value(forKey: "user")
                    print("user profile data",result!)
                
                    self.nameLbl.text =  (result as! NSDictionary).value(forKey: "name") as? String
                   
                    if ((result as! NSDictionary).value(forKey: "profile_pic") is NSNull)
                    {
                    }
                    else
                    {
                        self.profile = ((result as! NSDictionary).value(forKey: "profile_pic") as? String)!
                        print(self.profile)
                        if self.profile != "" {
                            
                            let url = URL(string:self.profile as String)!
                            print(url)
                            self.profileImg.af_setImage(withURL: url)
                        }
                        else
                        {
                            self.profileImg.image = UIImage(named :"placeMen")
                        }
                    }
                }
                else
                {
                    self.activityView.isHidden = true
                    let alert = UIAlertController(title: "Brexa", message: "failed to retrive data plese try again later..", preferredStyle: UIAlertControllerStyle.alert)
                    
                    alert.addAction(UIAlertAction(title:"Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated:true,completion: nil)
                }
            }
            else
            {
                //print(response.result.error!)
                self.activityView.isHidden = true
                let alert = UIAlertController(title: "Brexa", message: "Something went wrong please try again", preferredStyle: UIAlertControllerStyle.alert)
                
                alert.addAction(UIAlertAction(title:"Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated:true,completion: nil)
            }
        }
    }
    func logOutApi()
    {
        self.activityView.isHidden = false
        let headers = ["Content-Type": "application/json", "Authorization" :"Bearer \(tokenStr)"]
        Alamofire.request("http://vesnahealthsolutions.com/api/logout", method: HTTPMethod.get,  encoding: URLEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            if response.result.value != nil
            {
                if (response.result.value as! NSDictionary).value(forKey: "status") as! Bool == true
                {
                    self.activityView.isHidden = true
                    print(response.result.value!)
                    UserDefaults.standard.set(false, forKey: "isUserLoggedIn")
                     UserDefaults.standard.removeObject(forKey: "authorizationToken")
                    UserDefaults.standard.removeObject(forKey: "isUserSavedPin")
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignInVC") as! SignInVC
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                else
                {
                    self.activityView.isHidden = true
                    let msg = (response.result.value as! NSDictionary).value(forKey: "message") as! String
                    Global().showSimpleAlert(Title: "Brexa", message: msg, inClass: self)
                }
            }
            else
            {  //print(response.result.error!)
                self.activityView.isHidden = true
                let alert = UIAlertController(title: "Brexa", message: "Something went wrong please try again", preferredStyle: UIAlertControllerStyle.alert)
                
                alert.addAction(UIAlertAction(title:"Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated:true,completion: nil)
            }
        }
    }
    func BannerApi()
    {
        self.activityView.isHidden = false
        let headers = ["Content-Type": "application/json", "Authorization" :"Bearer \(tokenStr)"]
        Alamofire.request("http://vesnahealthsolutions.com/api/appBanner", method: HTTPMethod.get,  encoding: URLEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            if response.result.value != nil
            {
                if (response.result.value as! NSDictionary).value(forKey: "status") as! Bool == true
                {
                    self.activityView.isHidden = true
                    print(response.result.value!)
                    let result = (response.result.value as! NSDictionary)
                    self.bannerArray = (result as NSDictionary).value(forKey: "banner") as! NSArray
                    print("bannerArray",self.bannerArray)
                    self.HomeCollectionView.reloadData()
                    self.setTimer()
                }
                else
                {
                    self.activityView.isHidden = true
                    let msg = (response.result.value as! NSDictionary).value(forKey: "message") as! String
                    Global().showSimpleAlert(Title: "Brexa", message: msg, inClass: self)
                }
            }
            else
            {  //print(response.result.error!)
                self.activityView.isHidden = true
                let alert = UIAlertController(title: "Brexa", message: "Something went wrong please try again", preferredStyle: UIAlertControllerStyle.alert)
                
                alert.addAction(UIAlertAction(title:"Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated:true,completion: nil)
            }
        }
}
    func BlogsApi()
    {
        self.activityView.isHidden = false
        let headers = ["Content-Type": "application/json", "Authorization" :"Bearer \(tokenStr)"]
        Alamofire.request("http://vesnahealthsolutions.com/api/getBlogs", method: HTTPMethod.get,  encoding: URLEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            if response.result.value != nil
            {
                if (response.result.value as! NSDictionary).value(forKey: "success") as! Bool == true
                {
                    self.activityView.isHidden = true
                    print(response.result.value!)
                    let result = (response.result.value as! NSDictionary)
                    self.blogsArray = (result as NSDictionary).value(forKey: "blogs") as! NSArray
                    print("blogsArray",self.blogsArray)
                    self.BlogCollectionView.reloadData()
                }
                else
                {
                    self.activityView.isHidden = true
                    let msg = (response.result.value as! NSDictionary).value(forKey: "message") as! String
                    Global().showSimpleAlert(Title: "Brexa", message: msg, inClass: self)
                }
            }
            else
            {  //print(response.result.error!)
                self.activityView.isHidden = true
                let alert = UIAlertController(title: "Brexa", message: "Something went wrong please try again", preferredStyle: UIAlertControllerStyle.alert)
                
                alert.addAction(UIAlertAction(title:"Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated:true,completion: nil)
            }
        }
    }
}

