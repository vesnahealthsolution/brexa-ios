//
//  ProfileVC.swift
//  Brexa
//
//  Created by Jarvics on 03/11/17.
//  Copyright © 2017 Omninos_Solutions. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class ProfileVC: UIViewController {

    @IBOutlet weak var activityView: UIView!
    @IBOutlet var ShadowView: UIView!
    
    @IBOutlet weak var namelbl: UILabel!
    @IBOutlet weak var emailLbl: UILabel!
    @IBOutlet weak var phoneLBl: UILabel!
    @IBOutlet weak var maleLbl: UILabel!
    @IBOutlet weak var ageLBl: UILabel!
    @IBOutlet weak var profileImg: UIImageView!
    var profile = String()
    var tokenStr = String()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

         self.activityView.isHidden = true
        ShadowView.layer.shadowColor = UIColor.lightGray.cgColor
        ShadowView.layer.shadowOffset = CGSize(width:0,height: 1.5)
        ShadowView.layer.shadowRadius = 0
        ShadowView.layer.shadowOpacity = 1.0
        if UserDefaults.standard.value(forKey: "authorizationToken") != nil
        {
            tokenStr = UserDefaults.standard.value(forKey: "authorizationToken") as! String
            print(tokenStr)
        }
        else{
            print("no token found")
        }
         getProfileApi()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getProfileApi()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func back(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func editbtn(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "EditProfile") as! EditProfile
        vc.name = namelbl.text!
        vc.age = ageLBl.text!
        vc.gender = maleLbl.text!
        vc.profile = profile
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func getProfileApi()
    {

 self.activityView.isHidden = false
    let headers = ["Content-Type": "application/json", "Authorization" :"Bearer \(tokenStr)"]
    
    Alamofire.request("http://vesnahealthsolutions.com/api/profile", method: HTTPMethod.get,  encoding: URLEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
    if response.result.value != nil
    {
        if (response.result.value as! NSDictionary).value(forKey: "status") as! Bool == true
          {
             self.activityView.isHidden = true
    print(response.result.value!)
        let result = (response.result.value as! NSDictionary).value(forKey: "user")
        print("user profile data",result!)
            self.emailLbl.text = (result as! NSDictionary).value(forKey: "email") as? String
            self.phoneLBl.text = (result as! NSDictionary).value(forKey: "phone") as? String
            self.maleLbl.text = (result as! NSDictionary).value(forKey: "gender") as? String
            self.ageLBl.text = (result as! NSDictionary).value(forKey: "dob") as? String
            self.namelbl.text =  (result as! NSDictionary).value(forKey: "name") as? String
            if ((result as! NSDictionary).value(forKey: "profile_pic") is NSNull)
            {
            }
            else
            {
                self.profile = ((result as! NSDictionary).value(forKey: "profile_pic") as? String)!
                print(self.profile)
                if self.profile != "" {
                    
                    let url = URL(string:self.profile as String)!
                    print(url)
                    self.profileImg.af_setImage(withURL: url)
                }
                else
                {
                    self.profileImg.image = UIImage(named :"placeMen")
                }
            }
        }
        else
        {
            self.activityView.isHidden = true
            let alert = UIAlertController(title: "Brexa", message: "failed to retrive data plese try again later..", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title:"Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated:true,completion: nil)
        }
    }
    else
    {
    //print(response.result.error!)
        self.activityView.isHidden = true
        let alert = UIAlertController(title: "Brexa", message: "Something went wrong please try again", preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title:"Ok", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated:true,completion: nil)
    }
    }

    }
}

