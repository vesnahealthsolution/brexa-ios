//
//  EditProfile.swift
//  Brexa
//
//  Created by Jarvics on 03/11/17.
//  Copyright © 2017 Omninos_Solutions. All rights reserved.
//

import UIKit
import TextFieldEffects
import GoogleMaps
import GooglePlaces
import Alamofire

class EditProfile: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate, GMSMapViewDelegate,CLLocationManagerDelegate {

    @IBOutlet weak var activityView: UIView!
    @IBOutlet var Gnderview: UIView!
    @IBOutlet var nameview: UIView!
    var str2 = String()
    var ageint = Int()
    var name = String()
    var age = String()
    var profile = String()
    var gender = String()
    var imagePicker = UIImagePickerController()
    var imageData21 = Data()
    var locationManager = CLLocationManager()
    var manager = CLLocationManager()
    var location = CLLocationCoordinate2D()
    var currentLocation = CLLocationCoordinate2D()
    var dataImage = String()
    var fullBase64String = String()
    var jsonLocation = String()
    var lat = NSString()
    var lng = NSString()
    var dataImage1 = String()
    var tokenStr = String()
    
    @IBOutlet weak var profileImg: UIImageView!
    @IBOutlet weak var genderLbl: HoshiTextField!
    @IBOutlet weak var ageLbl: HoshiTextField!
    @IBOutlet var namelb: HoshiTextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        setStatusBarBackgroundColor(color: UIColor(red: 187/255.0, green: 42/255.0, blue: 105/255.0, alpha: 1))
        activityView.isHidden = true
        if self.profile != "" {
            
            let url = URL(string:self.profile as String)!
            print(url)
            self.profileImg.af_setImage(withURL: url)
            self.resizeImage(profileImg.image!, newHeight: 480)
            dataImage = imageData21.base64EncodedString(options: .lineLength64Characters)
            fullBase64String = dataImage
        }
        else
        {
            self.profileImg.image = UIImage(named :"placeMen")
        }
        if UserDefaults.standard.value(forKey: "authorizationToken") != nil
        {
            tokenStr = UserDefaults.standard.value(forKey: "authorizationToken") as! String
            print(tokenStr)
        }
        else{
            print("no token found")
        }
        namelb.text = name
        ageLbl.text = age
        genderLbl.text = gender
        
        
        Gnderview.layer.shadowColor = UIColor.lightGray.cgColor
        Gnderview.layer.shadowOffset = CGSize(width:0,height: 1.5)
        Gnderview.layer.shadowRadius = 0
        Gnderview.layer.shadowOpacity = 1.0
        
        Gnderview.layer.shadowColor = UIColor.lightGray.cgColor
        Gnderview.layer.shadowOffset = CGSize(width:0,height: 1.5)
        Gnderview.layer.shadowRadius = 0
        Gnderview.layer.shadowOpacity = 1.0
        self.locationManager.delegate = self
        self.locationManager.startUpdatingLocation()
        manager.delegate = self
        manager.requestWhenInUseAuthorization()
        locationManager.requestWhenInUseAuthorization()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.delegate = self
        self.locationManager.startUpdatingLocation()
    }
    func setStatusBarBackgroundColor(color: UIColor) {
        guard  let statusBar = (UIApplication.shared.value(forKey: "statusBarWindow") as AnyObject).value(forKey: "statusBar") as? UIView else {
            return
        }
        statusBar.backgroundColor = color
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]){
        currentLocation = (manager.location?.coordinate)!
        lat = String(currentLocation.latitude)as NSString
        lng = String(currentLocation.latitude)as NSString
        print(currentLocation)
        print(lat)
        print(lng)
        
        manager.stopUpdatingLocation()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func back(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func submitbtn(_ sender: Any) {
        
        let strage = ageLbl.text!
         ageint = (strage as NSString).integerValue
        print(ageint)
        let dict1 = ["lat":lat,"lng":lng]
        
        if let json = try? JSONSerialization.data(withJSONObject: dict1, options: []) {
            if String(data: json, encoding: String.Encoding.utf8) != nil {
                jsonLocation =  String(data: json, encoding: String.Encoding.utf8)!
                print("json data location",jsonLocation)
                str2 = jsonLocation.replacingOccurrences(of: "\\", with: "", options: .literal, range: nil)
                print(str2)
            }
        }        
        if (currentReachabilityStatus != .notReachable) == true
        {
            if fullBase64String == ""
            {
                let alert = UIAlertController(title: "Brexa", message: "profile picture can't be nil", preferredStyle: UIAlertControllerStyle.alert)
                
                alert.addAction(UIAlertAction(title:"Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated:true,completion: nil)
            }
            else
            {
            self.activityView.isHidden = false
            let dict = ["name":namelb.text!,"phone":"","dob":ageint,"gender":genderLbl.text!,"location":str2,"profile_pic": fullBase64String,"encoded":true] as [String : Any]
            
            self.activityView.isHidden = false
            let headers = ["Content-Type": "application/json", "Authorization" :"Bearer \(tokenStr)"]
            Alamofire.request("http://vesnahealthsolutions.com/api/profile/edit", method: HTTPMethod.post,parameters:dict,encoding: JSONEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
                print(response.result.value)
                if response.result.value != nil
                {
                    if (response.result.value as! NSDictionary).value(forKey: "status") as! Bool == true
                    {
                        self.activityView.isHidden = true
                        print(response.result.value!)
                        let result = (response.result.value as! NSDictionary).value(forKey: "user")
                        print("user profile data",result!)
                        let alert = UIAlertController(title: "Brexa", message: "profile updated succesfully..", preferredStyle: UIAlertControllerStyle.alert)
                        
                        alert.addAction(UIAlertAction(title:"Ok", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated:true,completion: nil)
                    }
                    else
                    {
                        self.activityView.isHidden = true
                        let alert = UIAlertController(title: "Brexa", message: "failed to retrive data plese try again later..", preferredStyle: UIAlertControllerStyle.alert)
                        
                        alert.addAction(UIAlertAction(title:"Ok", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated:true,completion: nil)
                    }
                }
                else
                {
                    //print(response.result.error!)
                    self.activityView.isHidden = true
                    let alert = UIAlertController(title: "Brexa", message: "Something went wrong please try again", preferredStyle: UIAlertControllerStyle.alert)
                    
                    alert.addAction(UIAlertAction(title:"Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated:true,completion: nil)
                }
            }
        }
        }
        else
        {
            print("No Connection Here")
            self.activityView.isHidden = true
            let alert = UIAlertController(title: "Brexa", message: "Internet Connection error..", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title:"Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated:true,completion: nil)
        }
    }
    @IBAction func pickImgBtn(_ sender: Any) {
        print("imageBtn")
        let actionSheet = UIAlertController.init(title: nil, message: "Pick From", preferredStyle: .actionSheet)
        let actionPhotoCamera = UIAlertAction(title: "Camera", style: .default) { (action) in
            print("cameraCoding")
            self.camera()
        }
        let actionLibrary1 = UIAlertAction(title: "Gallery", style: .default) { (action) in
            print("library")
            self.photoLibrary()
        }
        let okey = UIAlertAction(title: "cancel", style: .cancel)  { (action) in
            print("cancel")
        }
        actionSheet.addAction(okey)
        actionSheet.addAction(actionPhotoCamera)
        actionSheet.addAction(actionLibrary1)
        self.present(actionSheet, animated: true, completion: nil)
        
    }
    
    func photoLibrary()
    {
        let myPickerController = UIImagePickerController()
        myPickerController.delegate = self;
        myPickerController.sourceType = UIImagePickerControllerSourceType.photoLibrary
        myPickerController.mediaTypes = ["public.image", "public.movie"]
        self.present(myPickerController, animated: true, completion: nil)
    }
    func camera()
    {
        let picker = UIImagePickerController()
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            picker.allowsEditing = false
            picker.sourceType = UIImagePickerControllerSourceType.camera
            picker.cameraCaptureMode = .photo
            picker.delegate = self;
            
            picker.modalPresentationStyle = .fullScreen
            self.present(picker, animated: true, completion: nil)
        } else {
            noCamera()
        }
    }
    func noCamera(){
        let alertVC = UIAlertController(
            title: "No Camera",
            message: "Sorry, this device has no camera",
            preferredStyle: .alert)
        let okAction = UIAlertAction(
            title: "OK",
            style:.default,
            handler: nil)
        alertVC.addAction(okAction)
        present(
            alertVC,
            animated: true,
            completion: nil)
    }
    // MARK: - UIImagePickerController Datasources -
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        imagePicker.sourceType = UIImagePickerControllerSourceType.savedPhotosAlbum;
        profileImg.image = chosenImage
        self.resizeImage(chosenImage, newHeight: 480)
        dataImage = imageData21.base64EncodedString(options: .lineLength64Characters)
        fullBase64String = dataImage
        
        dismiss(animated: true, completion: nil)
    }
    func resizeImage(_ image: UIImage, newHeight: CGFloat) -> UIImage {
        let scale = newHeight / image.size.height
        let newWidth = image.size.width * scale
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        image.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        imageData21 = UIImageJPEGRepresentation(newImage!, 0.5)!  as Data
        UIGraphicsEndImageContext()
        return UIImage(data:imageData21 as Data)!
    }
    
}
