//
//  vouchersTableViewCell.swift
//  Brexa
//
//  Created by Jarvics  on 12/12/17.
//  Copyright © 2017 Omninos_Solutions. All rights reserved.
//

import UIKit

class vouchersTableViewCell: UITableViewCell {

    @IBOutlet weak var scanImg: UIImageView!
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var profileImg: UIImageView!
    
    @IBOutlet weak var codeLbl: UILabel!
    @IBOutlet weak var shareVoucherBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
