//
//  VoucherVC.swift
//  Brexa
//
//  Created by Jarvics on 07/11/17.
//  Copyright © 2017 Omninos_Solutions. All rights reserved.
//

import UIKit
import CoreImage
import Alamofire

class VoucherVC: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var noVoucherView: UIView!
    @IBOutlet weak var voucherTableView: UITableView!
    @IBOutlet weak var activityView: UIView!
    var tokenStr = String()
    var VoucherData = NSArray()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        noVoucherView.isHidden = true
        self.view.backgroundColor = UIColor(red: 187/255.0, green: 42/255.0, blue: 105/255.0, alpha: 1)
         self.activityView.isHidden = true
        if UserDefaults.standard.value(forKey: "authorizationToken") != nil
        {
            tokenStr = UserDefaults.standard.value(forKey: "authorizationToken") as! String
            print(tokenStr)
            
        }
        else{
            print("no token found")
            let alert = UIAlertController(title: "Invalid", message: "You must login first", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title:"Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated:true,completion: nil)
        }
        getProfileApi()
      
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }    
    @IBAction func BAckBtn(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return VoucherData.count
        }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "vouchersTableViewCell")as! vouchersTableViewCell
        
        if VoucherData.count != 0
        {
            cell.mainView.layer.shadowColor = UIColor.lightGray.cgColor
            cell.mainView.layer.shadowOffset = CGSize(width:0,height: 1.5)
            cell.mainView.layer.shadowRadius = 2.0
            cell.mainView.layer.shadowOpacity
                = 1.0
        cell.codeLbl.text = (VoucherData.value(forKey: "code")as! NSArray)[indexPath.row] as? String
        
                            if (VoucherData.value(forKey: "qr_code")as! NSArray)[indexPath.row] is NSNull
                            {
                            }
                            else
                            {
                                let profile = (VoucherData.value(forKey: "qr_code")as! NSArray)[indexPath.row] as? String
                                if profile != "" {
                                    let url = URL(string: profile!)!
                                    print(url)
                                    cell.scanImg.af_setImage(withURL: url)
                                }
                                else{
                                    cell.scanImg.image =  UIImage(named: "")
                                }
        
                            }
        }
        else
        {
            noVoucherView.isHidden = false
        }

        return cell
        }
    
    func generateQRCode(from string: String) -> UIImage?
    {
        let data = string.data(using: String.Encoding.ascii)
        
        if let filter = CIFilter(name: "CIQRCodeGenerator") {
            filter.setValue(data, forKey: "inputMessage")
            let transform = CGAffineTransform(scaleX: 3, y: 3)
            
            if let output = filter.outputImage?.transformed(by: transform) {
                return UIImage(ciImage: output)
            }
        }        
        return nil
    }
    
    func getProfileApi()
    {
        self.activityView.isHidden = false
        let headers = ["Content-Type": "application/json", "Authorization" :"Bearer \(tokenStr)"]
        
        Alamofire.request("http://vesnahealthsolutions.com/api/vouchers", method: HTTPMethod.get,  encoding: URLEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
            if response.result.value != nil
            {
                if (response.result.value as! NSDictionary).value(forKey: "status") as! Bool == true
                {
                    self.activityView.isHidden = true
                    print(response.result.value!)
                    let result = (response.result.value as! NSDictionary)
                    
                   print(result)
                    self.VoucherData =  (result).value(forKey: "vouchers") as! NSArray
                    if self.VoucherData.count == 0
                    {
                        self.noVoucherView.isHidden = false
                        self.voucherTableView.isHidden = true
                    }
                    self.voucherTableView.reloadData()
                }
                else
                {
                    self.activityView.isHidden = true
                    let alert = UIAlertController(title: "Brexa", message: "failed to retrive data plese try again later..", preferredStyle: UIAlertControllerStyle.alert)
                    
                    alert.addAction(UIAlertAction(title:"Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated:true,completion: nil)
                    self.noVoucherView.isHidden = false
                    self.voucherTableView.isHidden = true
                }
            }
            else
            {
                //print(response.result.error!)
                self.activityView.isHidden = true
                let alert = UIAlertController(title: "Brexa", message: "Something went wrong please try again", preferredStyle: UIAlertControllerStyle.alert)
                
                alert.addAction(UIAlertAction(title:"Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated:true,completion: nil)
            }
        }
    }

}
