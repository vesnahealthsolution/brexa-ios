//
//  enterPinViewController.swift
//  Brexa
//
//  Created by Jarvics  on 30/11/17.
//  Copyright © 2017 Omninos_Solutions. All rights reserved.
//

import UIKit
import Alamofire

class enterPinViewController: UIViewController {
    @IBOutlet weak var activityView: UIView!
    @IBOutlet weak var lbl1: UILabel!
    @IBOutlet weak var lbl2: UILabel!
    @IBOutlet weak var lbl3: UILabel!
    @IBOutlet weak var lbl4: UILabel!
    @IBOutlet weak var lbl5: UILabel!
    @IBOutlet weak var lbl6: UILabel!
    @IBOutlet weak var lbl7: UILabel!
    @IBOutlet weak var lbl8: UILabel!
    @IBOutlet weak var lbl9: UILabel!
    @IBOutlet weak var lbl0: UILabel!
    var profile = String()
    @IBOutlet weak var profileImg: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var pinTF: UITextField!
    var savedPin = String()
    var tokenStr = String()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

       self.view.backgroundColor = UIColor(red: 187/255.0, green: 42/255.0, blue: 105/255.0, alpha: 1)
        activityView.isHidden = true
       if UserDefaults.standard.value(forKey: "authorizationPin") !=  nil
        {
            savedPin = UserDefaults.standard.value(forKey: "authorizationPin") as! String
            print("Saved Pin",savedPin)
        }
       else{
        print("No pin found")
        }
        if UserDefaults.standard.value(forKey: "authorizationToken") != nil
        {
            tokenStr = UserDefaults.standard.value(forKey: "authorizationToken") as! String
            print(tokenStr)
        }
        else{
            print("no token found")
        }
        
         getProfileApi()
    }
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
 
    @IBAction func numberBtn(_ sender: UIButton) {
        
        if sender.tag == 1
        {
            checkPin(pin:lbl1.text!)
        }
        if sender.tag == 2
        {
             checkPin(pin: lbl2.text!)
        }
        if sender.tag == 3
        {
             checkPin(pin: lbl3.text!)
        }
        if sender.tag == 4
        {
             checkPin(pin: lbl4.text!)
        }
        if sender.tag == 5
        {
             checkPin(pin: lbl5.text!)
        }
        if sender.tag == 6
        {
             checkPin(pin: lbl6.text!)
        }
        if sender.tag == 7
        {
             checkPin(pin: lbl7.text!)
        }
        if sender.tag == 8
        {
            checkPin(pin: lbl8.text!)
        }
        if sender.tag == 9
        {
            checkPin(pin: lbl9.text!)
        }
        if sender.tag == 0
        {
            checkPin(pin: lbl0.text!)
        }
    }
    
    func checkPin(pin:String)
    {
        if (pinTF.text?.count)! <= 3
        {
            pinTF.text = "\(pinTF.text!)\(String(describing: pin))"
        
         if (pinTF.text?.count)! == 4{

            print(pinTF.text!)
            if pinTF.text == savedPin
            {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else
            {
                pinTF.text = ""
                let alert = UIAlertController(title: "Invalid", message: "You have entered wrong pin", preferredStyle: UIAlertControllerStyle.alert)
                
                alert.addAction(UIAlertAction(title:"Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated:true,completion: nil)
            }
        }
        }
        else {
            
        }
    }
    
    @IBAction func forgotPinBtn(_ sender: Any) {
    
        self.activityView.isHidden = false
        let headers = ["Content-Type": "application/json", "Authorization" :"Bearer \(tokenStr)"]
        Alamofire.request("http://vesnahealthsolutions.com/api/forgotPin", method: HTTPMethod.get,  encoding: URLEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            if response.result.value != nil
            {
                if (response.result.value as! NSDictionary).value(forKey: "status") as! Bool == true
                {
                    self.activityView.isHidden = true
                    print(response.result.value!)
                    let alert = UIAlertController(title: "Brexa", message: "PIN sent to Your mail", preferredStyle: UIAlertControllerStyle.alert)
                    
                    alert.addAction(UIAlertAction(title:"Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated:true,completion: nil)
                    
                }
                else
                {
                    self.activityView.isHidden = true
                    let msg = (response.result.value as! NSDictionary).value(forKey: "message") as! String
                    Global().showSimpleAlert(Title: "Brexa", message: msg, inClass: self)
                }
            }
            else
            {  //print(response.result.error!)
                self.activityView.isHidden = true
                let alert = UIAlertController(title: "Brexa", message: "Something went wrong please try again", preferredStyle: UIAlertControllerStyle.alert)
                
                alert.addAction(UIAlertAction(title:"Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated:true,completion: nil)
            }
        }
    }
    
    func getProfileApi()
    {
        self.activityView.isHidden = false
        let headers = ["Content-Type": "application/json", "Authorization" :"Bearer \(tokenStr)"]
        
        Alamofire.request("http://vesnahealthsolutions.com/api/profile", method: HTTPMethod.get,  encoding: URLEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            if response.result.value != nil
            {
                if (response.result.value as! NSDictionary).value(forKey: "status") as! Bool == true
                {
                    self.activityView.isHidden = true
                    print(response.result.value!)
                    let result = (response.result.value as! NSDictionary).value(forKey: "user")
                    print("user profile data",result!)
                    
                    self.nameLbl.text =  (result as! NSDictionary).value(forKey: "name") as? String
                   if ((result as! NSDictionary).value(forKey: "profile_pic") is NSNull)
                   {
                    }
                    else
                    {
                        self.profile = ((result as! NSDictionary).value(forKey: "profile_pic") as? String)!
                        print(self.profile)
                        if self.profile != "" {

                            let url = URL(string:self.profile as String)!
                            print(url)
                            self.profileImg.af_setImage(withURL: url)
                    }
                        else
                        {
                            self.profileImg.image = UIImage(named :"placeMen")
                        }
                    }
                }
                else
                {
                    self.activityView.isHidden = true
                    let alert = UIAlertController(title: "Brexa", message: "failed to retrive data plese try again later..", preferredStyle: UIAlertControllerStyle.alert)
                    
                    alert.addAction(UIAlertAction(title:"Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated:true,completion: nil)
                }
            }
            else
            {
                //print(response.result.error!)
                self.activityView.isHidden = true
                let alert = UIAlertController(title: "Brexa", message: "Something went wrong please try again", preferredStyle: UIAlertControllerStyle.alert)
                
                alert.addAction(UIAlertAction(title:"Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated:true,completion: nil)
            }
        }
    }
    }
