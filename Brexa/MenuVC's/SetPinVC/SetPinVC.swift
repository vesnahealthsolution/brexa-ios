//
//  SetPinVC.swift
//  Brexa
//
//  Created by Jarvics on 06/11/17.
//  Copyright © 2017 Omninos_Solutions. All rights reserved.
//

import UIKit
import Alamofire

class SetPinVC: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var activityView: UIView!
    @IBOutlet var ShadowView: UIView!
    @IBOutlet var Cforthtxt: UITextField!
    @IBOutlet var Cthrdtxt: UITextField!
    @IBOutlet var Csectxt: UITextField!
    @IBOutlet var Cfrsttxt: UITextField!
    @IBOutlet var Forthtxt: UITextField!
    @IBOutlet var Thrdtxt: UITextField!
    @IBOutlet var Sectxt: UITextField!
    @IBOutlet var Frsttxt: UITextField!
    var tokenStr = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setStatusBarBackgroundColor(color:UIColor(red: 187/255.0, green: 42/255.0, blue: 105/255.0, alpha: 1))
         self.activityView.isHidden = true
        ShadowView.layer.shadowColor = UIColor.lightGray.cgColor
        ShadowView.layer.shadowOffset = CGSize(width:0,height: 1.5)
        ShadowView.layer.shadowRadius = 2.0
        ShadowView.layer.shadowOpacity = 1.0
        Frsttxt.delegate = self
        Sectxt.delegate = self
        Thrdtxt.delegate = self
        Forthtxt.delegate = self
        Cfrsttxt.delegate = self
        Csectxt.delegate = self
        Cthrdtxt.delegate = self
        Cforthtxt.delegate = self
        Frsttxt.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControlEvents.editingChanged)
        Sectxt.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControlEvents.editingChanged)
        Thrdtxt.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControlEvents.editingChanged)
        Forthtxt.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControlEvents.editingChanged)
        Cfrsttxt.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControlEvents.editingChanged)
        Csectxt.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControlEvents.editingChanged)
        Cthrdtxt.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControlEvents.editingChanged)
        Cforthtxt.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControlEvents.editingChanged)
        if UserDefaults.standard.value(forKey: "authorizationToken") != nil
        {
            tokenStr = UserDefaults.standard.value(forKey: "authorizationToken") as! String
            print(tokenStr)
        }
        else{
            print("no token found")
        }
    }
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }
    func setStatusBarBackgroundColor(color: UIColor) {
        guard  let statusBar = (UIApplication.shared.value(forKey: "statusBarWindow") as AnyObject).value(forKey: "statusBar") as? UIView else {
            return
        }
        statusBar.backgroundColor = color
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        Frsttxt.becomeFirstResponder()
        
    }
    @objc func textFieldDidChange(textField: UITextField) {
        let text = textField.text
        
        if text?.utf16.count==1{
            switch textField{
            case Frsttxt:
                Sectxt.becomeFirstResponder()
            case Sectxt:
                Thrdtxt.becomeFirstResponder()
            case Thrdtxt:
                Forthtxt.becomeFirstResponder()
            case Forthtxt:
                Forthtxt.resignFirstResponder()
            default:
                break
            }
        }else{
            
        }
        if text?.utf16.count==1{
            switch textField{
            case Cfrsttxt:
                Csectxt.becomeFirstResponder()
            case Csectxt:
                Cthrdtxt.becomeFirstResponder()
            case Cthrdtxt:
                Cforthtxt.becomeFirstResponder()
            case Cforthtxt:
                Cforthtxt.resignFirstResponder()
            default:
                break
            }
        }else{
            
        }
    }
    
    @IBAction func Submitbtn(_ sender: Any) {
        
        let pin1 = "\(Frsttxt.text!)\(Sectxt.text!)\(Thrdtxt.text!)\(Forthtxt.text!)"
        
        let pin2 = "\(Cfrsttxt.text!)\(Csectxt.text!)\(Cthrdtxt.text!)\(Cforthtxt.text!)"
        
        if pin1 != pin2
        {
            let alert = UIAlertController(title: "Brexa", message: "Pin did't match", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title:"Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated:true,completion: nil)
        }
        
        else{
            if (currentReachabilityStatus != .notReachable) == true
            {
                self.activityView.isHidden = false
                let dict = ["email":pin1] as [String : Any]
                
                self.activityView.isHidden = false
                let headers = ["Content-Type": "application/json", "Authorization" :"Bearer \(tokenStr)"]
                
                Alamofire.request("http://vesnahealthsolutions.com/api/setPin", method: HTTPMethod.post,parameters:dict,encoding: JSONEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
                    print(response.result.value)
                    if response.result.value != nil
                    {
                        if (response.result.value as! NSDictionary).value(forKey: "status") as! Bool == true
                        {
                            self.activityView.isHidden = true
                            print(response.result.value!)
                            UserDefaults.standard.set(pin1, forKey: "authorizationPin")
                            UserDefaults.standard.set(true, forKey: "isUserSavedPin")
                            UserDefaults.standard.synchronize()
                            let vc = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                        else
                        {
                            self.activityView.isHidden = true
                            let alert = UIAlertController(title: "Brexa", message: "failed to retrive data plese try again later..", preferredStyle: UIAlertControllerStyle.alert)
                            
                            alert.addAction(UIAlertAction(title:"Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated:true,completion: nil)
                        }
                    }
                    else
                    {
                        //print(response.result.error!)
                        self.activityView.isHidden = true
                        let alert = UIAlertController(title: "Brexa", message: "Something went wrong please try again", preferredStyle: UIAlertControllerStyle.alert)
                        
                        alert.addAction(UIAlertAction(title:"Ok", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated:true,completion: nil)
                    }
                }
            }
            else
            {
                print("No Connection Here")
                self.activityView.isHidden = true
                let alert = UIAlertController(title: "Brexa", message: "Internet Connection error..", preferredStyle: UIAlertControllerStyle.alert)
                
                alert.addAction(UIAlertAction(title:"Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated:true,completion: nil)
            }
        }
    }
    
    @IBAction func Skipbtn(_ sender: Any) {
        
    }
    @IBAction func backbtn(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
}
