//
//  DiagDetailVC.swift
//  Brexa
//
//  Created by Jarvics on 06/11/17.
//  Copyright © 2017 Omninos_Solutions. All rights reserved.
//

import UIKit

class DiagDetailVC: UIViewController {

    @IBOutlet var ShadowView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
       setStatusBarBackgroundColor(color: UIColor(red: 187/255.0, green: 42/255.0, blue: 105/255.0, alpha: 1))
        ShadowView.layer.shadowColor = UIColor.lightGray.cgColor
        ShadowView.layer.shadowOffset = CGSize(width:0,height: 1.5)
        ShadowView.layer.shadowRadius = 0
        ShadowView.layer.shadowOpacity = 1.0
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func setStatusBarBackgroundColor(color: UIColor) {
        guard  let statusBar = (UIApplication.shared.value(forKey: "statusBarWindow") as AnyObject).value(forKey: "statusBar") as? UIView else {
            return
        }
        statusBar.backgroundColor = color
    }
    
    @IBAction func backbtn(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func morebtn(_ sender: Any) {
    }
    
    

}
