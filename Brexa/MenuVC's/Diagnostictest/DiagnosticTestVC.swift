//
//  DiagnosticTestVC.swift
//  Brexa
//
//  Created by Jarvics on 06/11/17.
//  Copyright © 2017 Omninos_Solutions. All rights reserved.
//

import UIKit

class DiagnosticTestVC: UIViewController,UITableViewDelegate,UITableViewDataSource{

    @IBOutlet var DiagtableView: UITableView!
    var tiltimg : NSArray = ["mammogram_device","mri","breast","genetic","ultra_sound"]
    var titllbl : NSArray = ["Mammography","Breast MRI","Breast Biopsy","Genetic Test","Breast Ultra Sound"]
    override func viewDidLoad() {
        super.viewDidLoad()
   setStatusBarBackgroundColor(color: UIColor(red: 187/255.0, green: 42/255.0, blue: 105/255.0, alpha: 1))
      
    }
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }
    func setStatusBarBackgroundColor(color: UIColor) {
        guard  let statusBar = (UIApplication.shared.value(forKey: "statusBarWindow") as AnyObject).value(forKey: "statusBar") as? UIView else {
            return
        }
        statusBar.backgroundColor = color
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titllbl.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = DiagtableView.dequeueReusableCell(withIdentifier: "DiagTableViewCell")as! DiagTableViewCell
        cell.titleimag.image = UIImage(named: tiltimg[indexPath.row] as! String)
        cell.Titlelabl.text = self.titllbl[indexPath.row] as? String
        cell.ShadowView.layer.shadowColor = UIColor.lightGray.cgColor
        cell.ShadowView.layer.shadowOffset = CGSize(width:0,height: 1.5)
        cell.ShadowView.layer.shadowRadius = 2.0
        cell.ShadowView.layer.shadowOpacity = 1.0
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "DiagDetailVC") as! DiagDetailVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func Backbtn(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    @IBAction func morebtn(_ sender: Any) {
        
        
    }
    

}
