//
//  DiagTableViewCell.swift
//  Brexa
//
//  Created by Jarvics on 06/11/17.
//  Copyright © 2017 Omninos_Solutions. All rights reserved.
//

import UIKit

class DiagTableViewCell: UITableViewCell {
    @IBOutlet var titleimag: UIImageView!
    @IBOutlet var Titlelabl: UILabel!
    @IBOutlet var ShadowView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
