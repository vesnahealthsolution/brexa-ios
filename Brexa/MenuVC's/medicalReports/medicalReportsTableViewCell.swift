//
//  medicalReportsTableViewCell.swift
//  Brexa
//
//  Created by Jarvics  on 14/12/17.
//  Copyright © 2017 Omninos_Solutions. All rights reserved.
//

import UIKit

class medicalReportsTableViewCell: UITableViewCell {

    @IBOutlet weak var mainImg: UIImageView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var hospitalLbl: UILabel!
    @IBOutlet weak var dataLbl: UILabel!
    @IBOutlet weak var mamogramLbl: UILabel!
    @IBOutlet weak var viewfullreportBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
