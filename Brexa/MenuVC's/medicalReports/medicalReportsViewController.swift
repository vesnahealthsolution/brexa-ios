//
//  medicalReportsViewController.swift
//  Brexa
//
//  Created by Jarvics  on 14/12/17.
//  Copyright © 2017 Omninos_Solutions. All rights reserved.
//

import UIKit
import Alamofire

class medicalReportsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var noReportView: UIView!
    @IBOutlet weak var MedicalTableView: UITableView!
    @IBOutlet weak var activityView: UIView!
    var MedicalData = NSArray()
    var tokenStr = String()
    override func viewDidLoad() {
        super.viewDidLoad()

         self.noReportView.isHidden = true
        self.view.backgroundColor = UIColor(red: 187/255.0, green: 42/255.0, blue: 105/255.0, alpha: 1)
        activityView.isHidden = true
        if UserDefaults.standard.value(forKey: "authorizationToken") != nil
        {
            tokenStr = UserDefaults.standard.value(forKey: "authorizationToken") as! String
            print(tokenStr)
        }
        else{
            print("no token found")
            let alert = UIAlertController(title: "Invalid", message: "You must login first", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title:"Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated:true,completion: nil)
        }
         getMedicalReportsApi()
        
    }
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return MedicalData.count
    }
    @IBAction func backBtn(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "medicalReportsTableViewCell")as! medicalReportsTableViewCell
        if MedicalData.count != 0
        {            
        cell.mainView.layer.shadowColor = UIColor.lightGray.cgColor
        cell.mainView.layer.shadowOffset = CGSize(width:0,height: 1.5)
        cell.mainView.layer.shadowRadius = 2.0
        cell.mainView.layer.shadowOpacity
                = 1.0
        cell.mamogramLbl.text = (MedicalData.value(forKey: "title")as! NSArray)[indexPath.row] as? String
        cell.dataLbl.text = (MedicalData.value(forKey: "date")as! NSArray)[indexPath.row] as? String
        cell.hospitalLbl.text = (MedicalData.value(forKey: "doc")as! NSArray)[indexPath.row] as? String
        }
        
        else
        {
            self.noReportView.isHidden = false
            self.MedicalTableView.isHidden = true
        }

//        if (MedicalData.value(forKey: "image_url")as! NSArray)[indexPath.row] is NSNull
//        {
//        }
//        else
//        {
//            let profile = (MedicalData.value(forKey: "image_url")as! NSArray)[indexPath.row] as? String
//            if profile != "" {
//                
//                let url1 = URL(string: profile!)!
//                 cell.mainImg.image = drawPDFfromURL(url: url1)
//                print(cell.mainImg.image!)
//               // cell.mainImg.af_setImage(withURL: url)
//            }
//            else{
//                cell.mainImg.image =  UIImage(named: "")
//            }
//        }

        return cell
    }
    func getMedicalReportsApi()
    {
        self.activityView.isHidden = false
        let headers = ["Content-Type": "application/json", "Authorization" :"Bearer \(tokenStr)"]
        
        Alamofire.request("http://vesnahealthsolutions.com/api/getReports", method: HTTPMethod.get,  encoding: URLEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
            if response.result.value != nil
            {
                if (response.result.value as! NSDictionary).value(forKey: "status") as! Bool == true
                {
                    self.activityView.isHidden = true
                    print(response.result.value!)
                    let result = (response.result.value as! NSDictionary)
                    
                    print(result)
                    self.MedicalData =  (result).value(forKey: "reports") as! NSArray
                    if self.MedicalData.count == 0
                    {
                        self.noReportView.isHidden = false
                        self.MedicalTableView.isHidden = true
                    }
                    self.MedicalTableView.reloadData()
                }
                else
                {
                    self.activityView.isHidden = true
                    let alert = UIAlertController(title: "Brexa", message: "failed to retrive data plese try again later..", preferredStyle: UIAlertControllerStyle.alert)
                    
                    alert.addAction(UIAlertAction(title:"Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated:true,completion: nil)
//                    self.noReportView.isHidden = false
//                    self.MedicalTableView.isHidden = true
                }
            }
            else
            {
                //print(response.result.error!)
                self.activityView.isHidden = true
                let alert = UIAlertController(title: "Brexa", message: "Something went wrong please try again", preferredStyle: UIAlertControllerStyle.alert)
                
                alert.addAction(UIAlertAction(title:"Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated:true,completion: nil)
            }
        }
    }
    
    func drawPDFfromURL(url: URL) -> UIImage? {
        guard let document = CGPDFDocument(url as CFURL) else { return nil }
        guard let page = document.page(at: 1) else { return nil }
        
        let pageRect = page.getBoxRect(.mediaBox)
        let renderer = UIGraphicsImageRenderer(size: pageRect.size)
        let img = renderer.image { ctx in
            UIColor.white.set()
            ctx.fill(pageRect)
            
            ctx.cgContext.translateBy(x: 0.0, y: pageRect.size.height)
            ctx.cgContext.scaleBy(x: 1.0, y: -1.0)
            
            ctx.cgContext.drawPDFPage(page)
        }
        return img
    }
    
}
