//
//  BookMammogramVC.swift
//  Brexa
//
//  Created by Jarvics on 01/11/17.
//  Copyright © 2017 Omninos_Solutions. All rights reserved.
//

import UIKit
import CoreLocation

class BookMammogramVC: UIViewController,UITableViewDataSource,UITableViewDelegate,UIScrollViewDelegate,CLLocationManagerDelegate {
    @IBOutlet var ratedView: UIView!
    @IBOutlet var nearView: UIView!
    @IBOutlet var partView: UIView!
    @IBOutlet var BooktableView: UITableView!
    @IBOutlet var NeartableView: UITableView!
    @IBOutlet var MosttableView: UITableView!
    @IBOutlet var ScrollView: UIScrollView!
    @IBOutlet weak var activityView: UIView!
    var mammogramData = NSArray()
    var mammogramData1 = NSArray()
    var mammogramData2 = NSArray()
    var num = Int()
    var manager = CLLocationManager()
    var currentLocation = CLLocationCoordinate2D()
    override func viewDidLoad() {
        super.viewDidLoad()
         self.view.backgroundColor = UIColor(red: 187/255.0, green: 42/255.0, blue: 105/255.0, alpha: 1)
        num = 1
        activityView.isHidden = true
        manager.delegate = self
        manager.requestWhenInUseAuthorization()
        manager.startUpdatingLocation()
        ScrollView.isScrollEnabled = true
        ScrollView.delegate = self
        partView.isHidden = false
        nearView.isHidden = true
        ratedView.isHidden = true
         GetMammoProfile()
        
    }
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }

//    override func viewDidLayoutSubviews() {
//        ScrollView.contentSize = CGSize(width: self.view.frame.size.width * 3, height: 0)
//    }
    
    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]){
        currentLocation = (manager.location?.coordinate)!
        print(currentLocation)
        manager.stopUpdatingLocation()
       
    }
    func resetTabs() {
        partView.isHidden = true
        nearView.isHidden = true
        ratedView.isHidden = true
    }
//    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//         resetTabs()
//        if scrollView.contentOffset.x == 0 {
//            partView.isHidden = false
//            nearView.isHidden = true
//            ratedView.isHidden = true
//            //GetMammoProfile()
//        }
//        else if scrollView.contentOffset.x > view.frame.size.width && scrollView.contentOffset.x <= view.frame.size.width * 2  {
//            partView.isHidden = true
//            nearView.isHidden = false
//            ratedView.isHidden = true
//            // GetNearMammo()
//          }
//        else if scrollView.contentOffset.x > view.frame.size.width && scrollView.contentOffset.x <= view.frame.size.width * 3 {
//            partView.isHidden = true
//            nearView.isHidden = true
//            ratedView.isHidden = false
//            // GetTopMammo()
//        }
//    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       if tableView == BooktableView{
        return mammogramData.count
        }else if tableView == NeartableView{
        return  mammogramData1.count
       }else{
        return mammogramData2.count
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == BooktableView{
        let cell = BooktableView.dequeueReusableCell(withIdentifier: "BookmamTableViewCell")as! BookmamTableViewCell
            
            cell.mainView.layer.shadowColor = UIColor.lightGray.cgColor
            cell.mainView.layer.shadowOffset = CGSize(width:0,height: 1.5)
            cell.mainView.layer.shadowRadius = 2.0
            cell.mainView.layer.shadowOpacity = 1.0
            cell.mammoName.text = (mammogramData.value(forKey: "name")as! NSArray)[indexPath.row] as? String
            
            let blogprofile2 = (mammogramData.value(forKey: "profile_pic")as! NSArray)[indexPath.row] as? String
            
            if ((mammogramData.value(forKey: "rating")as! NSArray)[indexPath.row] is NSNull)
            {cell.star1.image = UIImage(named: "star")
                cell.star2.image = UIImage(named: "star")
                cell.star3.image = UIImage(named: "star")
                cell.star4.image = UIImage(named: "star")
                cell.star5.image = UIImage(named: "star")
            }
            else{
                let ratingNum = (mammogramData.value(forKey: "rating")as! NSArray)[indexPath.row] as? NSNumber
                let rateDouble = Double(ratingNum!)
                let ratenew = round(rateDouble)
                if ratenew == 0{
                    cell.star1.image = UIImage(named: "star")
                    cell.star2.image = UIImage(named: "star")
                    cell.star3.image = UIImage(named: "star")
                    cell.star4.image = UIImage(named: "star")
                    cell.star5.image = UIImage(named: "star")
                }
                else if ratenew == 1{
                    cell.star1.image = UIImage(named: "ratestar")
                    cell.star2.image = UIImage(named: "star")
                    cell.star3.image = UIImage(named: "star")
                    cell.star4.image = UIImage(named: "star")
                    cell.star5.image = UIImage(named: "star")
                }
                else if ratenew == 2{
                    cell.star1.image = UIImage(named: "ratestar")
                    cell.star2.image = UIImage(named: "ratestar")
                    cell.star3.image = UIImage(named: "star")
                    cell.star4.image = UIImage(named:"star")
                    cell.star5.image = UIImage(named: "star")
                    
                }
                else if ratenew == 3{
                    cell.star1.image = UIImage(named: "ratestar")
                    cell.star2.image = UIImage(named: "ratestar")
                    cell.star3.image = UIImage(named: "ratestar")
                    cell.star4.image = UIImage(named: "star")
                    cell.star5.image = UIImage(named: "star")
                    
                }
                    
                else if ratenew == 4{
                    cell.star1.image = UIImage(named: "ratestar")
                    cell.star2.image = UIImage(named: "ratestar")
                    cell.star3.image = UIImage(named: "ratestar")
                    cell.star4.image = UIImage(named: "ratestar")
                    cell.star5.image = UIImage(named: "star")
                }
                    
                else if ratenew == 5{
                    cell.star1.image = UIImage(named: "ratestar")
                    cell.star2.image = UIImage(named: "ratestar")
                    cell.star3.image = UIImage(named: "ratestar")
                    cell.star4.image = UIImage(named: "ratestar")
                    cell.star5.image = UIImage(named: "ratestar")
                }
            }
            
            if blogprofile2 != "" {
                let url = URL(string: blogprofile2!)!
                print(url)
                cell.mammoImg.af_setImage(withURL: url)
            }
            else{
                cell.mammoImg.image =  UIImage(named: "Mamogram lab")
            }
            
        return cell
        }else if tableView == NeartableView{
            let cell = NeartableView.dequeueReusableCell(withIdentifier: "NearbyTableViewCell")as! NearbyTableViewCell
            
            cell.mainView.layer.shadowColor = UIColor.lightGray.cgColor
            cell.mainView.layer.shadowOffset = CGSize(width:0,height: 1.5)
            cell.mainView.layer.shadowRadius = 2.0
            cell.mainView.layer.shadowOpacity
                = 1.0
            cell.mammoName.text =  (mammogramData1.value(forKey: "name")as! NSArray)[indexPath.row] as? String
            
            let blogprofile2 = (mammogramData1.value(forKey: "profile_pic")as! NSArray)[indexPath.row] as? String
            
            if ((mammogramData1.value(forKey: "rating")as! NSArray)[indexPath.row] is NSNull)
            {
                cell.star1.image = UIImage(named: "star")
                cell.star2.image = UIImage(named: "star")
                cell.star3.image = UIImage(named: "star")
                cell.star4.image = UIImage(named: "star")
                cell.star5.image = UIImage(named: "star")
            }
            else{
                let ratingNum = (mammogramData1.value(forKey: "rating")as! NSArray)[indexPath.row] as? NSNumber
                
                let rateDouble = Double(ratingNum!)
                let ratenew = round(rateDouble)
                
                if ratenew == 0{
                    cell.star1.image = UIImage(named: "star")
                    cell.star2.image = UIImage(named: "star")
                    cell.star3.image = UIImage(named: "star")
                    cell.star4.image = UIImage(named: "star")
                    cell.star5.image = UIImage(named: "star")
                }
                else if ratenew == 1{
                    cell.star1.image = UIImage(named: "ratestar")
                    cell.star2.image = UIImage(named: "star")
                    cell.star3.image = UIImage(named: "star")
                    cell.star4.image = UIImage(named: "star")
                    cell.star5.image = UIImage(named: "star")
                }
                else if ratenew == 2{
                    cell.star1.image = UIImage(named: "ratestar")
                    cell.star2.image = UIImage(named: "ratestar")
                    cell.star3.image = UIImage(named: "star")
                    cell.star4.image = UIImage(named:"star")
                    cell.star5.image = UIImage(named: "star")
                    
                }
                else if ratenew == 3{
                    cell.star1.image = UIImage(named: "ratestar")
                    cell.star2.image = UIImage(named: "ratestar")
                    cell.star3.image = UIImage(named: "ratestar")
                    cell.star4.image = UIImage(named: "star")
                    cell.star5.image = UIImage(named: "star")
                    
                }
                    
                else if ratenew == 4{
                    cell.star1.image = UIImage(named: "ratestar")
                    cell.star2.image = UIImage(named: "ratestar")
                    cell.star3.image = UIImage(named: "ratestar")
                    cell.star4.image = UIImage(named: "ratestar")
                    cell.star5.image = UIImage(named: "star")
                }
                    
                else if ratenew == 5{
                    cell.star1.image = UIImage(named: "ratestar")
                    cell.star2.image = UIImage(named: "ratestar")
                    cell.star3.image = UIImage(named: "ratestar")
                    cell.star4.image = UIImage(named: "ratestar")
                    cell.star5.image = UIImage(named: "ratestar")
                }
            }
            
            
            
            if blogprofile2 != "" {
                let url = URL(string: blogprofile2!)!
                print(url)
                cell.mammoImg.af_setImage(withURL: url)
            }
            else{
                cell.mammoImg.image =  UIImage(named: "Mamogram lab")
            }
            return cell
        }else{
            let cell = MosttableView.dequeueReusableCell(withIdentifier: "MostRatedTableViewCell")as! MostRatedTableViewCell
            cell.mainView.layer.shadowColor = UIColor.lightGray.cgColor
            cell.mainView.layer.shadowOffset = CGSize(width:0,height: 1.5)
            cell.mainView.layer.shadowRadius = 2.0
            cell.mainView.layer.shadowOpacity
                = 1.0
            cell.mammoLbl.text =  (mammogramData2.value(forKey: "name")as! NSArray)[indexPath.row] as? String
            let blogprofile2 = (mammogramData2.value(forKey: "profile_pic")as! NSArray)[indexPath.row] as? String
            
            if ((mammogramData2.value(forKey: "rating")as! NSArray)[indexPath.row] is NSNull)
            {cell.star1.image = UIImage(named: "star")
                cell.star2.image = UIImage(named: "star")
                cell.star3.image = UIImage(named: "star")
                cell.star4.image = UIImage(named: "star")
                cell.star5.image = UIImage(named: "star")
            }
            else{
                let ratingNum = (mammogramData2.value(forKey: "rating")as! NSArray)[indexPath.row] as? NSNumber
                let rateDouble = Double(ratingNum!)
                let ratenew = round(rateDouble)
                if ratenew == 0{
                    cell.star1.image = UIImage(named: "star")
                    cell.star2.image = UIImage(named: "star")
                    cell.star3.image = UIImage(named: "star")
                    cell.star4.image = UIImage(named: "star")
                    cell.star5.image = UIImage(named: "star")
                }
                else if ratenew == 1{
                    cell.star1.image = UIImage(named: "ratestar")
                    cell.star2.image = UIImage(named: "star")
                    cell.star3.image = UIImage(named: "star")
                    cell.star4.image = UIImage(named: "star")
                    cell.star5.image = UIImage(named: "star")
                }
                else if ratenew == 2{
                    cell.star1.image = UIImage(named: "ratestar")
                    cell.star2.image = UIImage(named: "ratestar")
                    cell.star3.image = UIImage(named: "star")
                    cell.star4.image = UIImage(named:"star")
                    cell.star5.image = UIImage(named: "star")
                    
                }
                else if ratenew == 3{
                    cell.star1.image = UIImage(named: "ratestar")
                    cell.star2.image = UIImage(named: "ratestar")
                    cell.star3.image = UIImage(named: "ratestar")
                    cell.star4.image = UIImage(named: "star")
                    cell.star5.image = UIImage(named: "star")
                    
                }
                    
                else if ratenew == 4{
                    cell.star1.image = UIImage(named: "ratestar")
                    cell.star2.image = UIImage(named: "ratestar")
                    cell.star3.image = UIImage(named: "ratestar")
                    cell.star4.image = UIImage(named: "ratestar")
                    cell.star5.image = UIImage(named: "star")
                }
                    
                else if ratenew == 5{
                    cell.star1.image = UIImage(named: "ratestar")
                    cell.star2.image = UIImage(named: "ratestar")
                    cell.star3.image = UIImage(named: "ratestar")
                    cell.star4.image = UIImage(named: "ratestar")
                    cell.star5.image = UIImage(named: "ratestar")
                }
                
            }
            if blogprofile2 != "" {
                let url = URL(string: blogprofile2!)!
                print(url)
                cell.mammoImg.af_setImage(withURL: url)
            }
            else{
                cell.mammoImg.image =  UIImage(named: "Mamogram lab")
            }
            return cell
            
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
          if tableView == BooktableView{
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "doctorDetailViewController") as! doctorDetailViewController
            if ((mammogramData.value(forKey: "rating")as! NSArray)[indexPath.row] is NSNull)
            {
            }
            else{
                let ratingNum = (mammogramData.value(forKey: "rating")as! NSArray)[indexPath.row] as? NSNumber
                let rateDouble = Double(ratingNum!)
                
              vc.rate = rateDouble
            }
            if ((mammogramData.value(forKey: "description")as! NSArray)[indexPath.row] is NSNull)
            {
                vc.about = ""
            }
            else
            {
            vc.about = ((mammogramData.value(forKey: "description")as! NSArray)[indexPath.row] as? String)!
            }
            
            if ((mammogramData.value(forKey: "phone")as! NSArray)[indexPath.row] is NSNull)
            {
                vc.Phone = ""
            }
            else
            {
                vc.Phone = ((mammogramData.value(forKey: "phone")as! NSArray)[indexPath.row] as? String)!
            }
            if ((mammogramData.value(forKey: "place")as! NSArray)[indexPath.row] is NSNull)
            {
               vc.address = ""
            }
            else
            {
            vc.address = ((mammogramData.value(forKey: "place")as! NSArray)[indexPath.row] as? String)!
            }
            if ((mammogramData.value(forKey: "name")as! NSArray)[indexPath.row] is NSNull)
            {
                 vc.name = ""
            }
            else
            {
            vc.name = ((mammogramData.value(forKey: "name")as! NSArray)[indexPath.row] as? String)!
            }
            if ((mammogramData.value(forKey: "profile_pic")as! NSArray)[indexPath.row] is NSNull)
            {
            }
            else
            {
            vc.profile = ((mammogramData.value(forKey: "profile_pic")as! NSArray)[indexPath.row] as? String)!
            }
            
        self.navigationController?.pushViewController(vc, animated: true)
        }
            
       else if tableView == NeartableView{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "doctorDetailViewController") as! doctorDetailViewController
            if ((mammogramData1.value(forKey: "rating")as! NSArray)[indexPath.row] is NSNull)
            {
            }
            else{
                let ratingNum = (mammogramData1.value(forKey: "rating")as! NSArray)[indexPath.row] as? NSNumber
                let rateDouble = Double(ratingNum!)
                
                vc.rate = rateDouble
            }
            
            
            if ((mammogramData1.value(forKey: "phone")as! NSArray)[indexPath.row] is NSNull)
            {
                vc.Phone = ""
            }
            else
            {
                vc.Phone = ((mammogramData1.value(forKey: "phone")as! NSArray)[indexPath.row] as? String)!
            }
            vc.about = ((mammogramData1.value(forKey: "description")as! NSArray)[indexPath.row] as? String)!
            vc.address = ((mammogramData1.value(forKey: "place")as! NSArray)[indexPath.row] as? String)!
            vc.name = ((mammogramData1.value(forKey: "name")as! NSArray)[indexPath.row] as? String)!
            if ((mammogramData1.value(forKey: "profile_pic")as! NSArray)[indexPath.row] is NSNull)
            {
            }
            else
            {
                vc.profile = ((mammogramData1.value(forKey: "profile_pic")as! NSArray)[indexPath.row] as? String)!
            }
            self.navigationController?.pushViewController(vc, animated: true)
       }
        else
          {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "doctorDetailViewController") as! doctorDetailViewController
            if ((mammogramData2.value(forKey: "rating")as! NSArray)[indexPath.row] is NSNull)
            {
                vc.rate = 0.0
            }
            else{
                let ratingNum = (mammogramData2.value(forKey: "rating")as! NSArray)[indexPath.row] as? NSNumber
                let rateDouble = Double(ratingNum!)
                
                vc.rate = rateDouble
            }
            
            
            if ((mammogramData2.value(forKey: "phone")as! NSArray)[indexPath.row] is NSNull)
            {
                vc.Phone = ""
            }
            else
            {
                vc.Phone = ((mammogramData2.value(forKey: "phone")as! NSArray)[indexPath.row] as? String)!
            }
            vc.about = ((mammogramData2.value(forKey: "description")as! NSArray)[indexPath.row] as? String)!
            vc.address = ((mammogramData2.value(forKey: "place")as! NSArray)[indexPath.row] as? String)!
            vc.name = ((mammogramData2.value(forKey: "name")as! NSArray)[indexPath.row] as? String)!
            if ((mammogramData2.value(forKey: "profile_pic")as! NSArray)[indexPath.row] is NSNull)
            {
            }
            else
            {
                vc.profile = ((mammogramData2.value(forKey: "profile_pic")as! NSArray)[indexPath.row] as? String)!
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }
            
        }
    
    
    @IBAction func Backbtn(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
        
    }
    @IBAction func selection(_ sender: UIButton) {
        if sender.tag == 0
        {
            partView.isHidden = false
            nearView.isHidden = true
            ratedView.isHidden = true
            self.ScrollView.contentOffset.x = 0
            print(self.ScrollView.contentOffset.x)
            GetMammoProfile()
        }
        else if sender.tag == 1
        {
            partView.isHidden = true
            nearView.isHidden = false
            ratedView.isHidden = true
            self.ScrollView.contentOffset.x = self.ScrollView.frame.width
        print(self.ScrollView.contentOffset.x)
            print(nearView.frame.origin.x)
            GetNearMammo()
        }
       else if sender.tag == 2
        {
            partView.isHidden = true
            nearView.isHidden = true
            ratedView.isHidden = false
            self.ScrollView.contentOffset.x = self.ScrollView.frame.width * 2
              print(self.ScrollView.contentOffset.x)
            GetTopMammo()
        }
        
    }
    func GetMammoProfile()
    {
        activityView.isHidden = false
        let userinfo:NSDictionary = [:]
        
        Global().alamofire_withGetMethod("doc/partners", parameters: userinfo, completion: { (result) in
            print("Product data",result)
            let msg = (result as? NSDictionary)?.value(forKey: "message") as! String?
            if (result as! NSDictionary).value(forKey: "status") as? Bool == true
            {
                self.activityView.isHidden = true
                 self.mammogramData =  (result as! NSDictionary).value(forKey: "doc") as! NSArray
                
                print("get mammogram data",self.mammogramData)
                self.BooktableView.reloadData()
                
            }
            else{
                self.activityView.isHidden = true
                Global().showSimpleAlert(Title: "Hashy", message: msg!, inClass: self)
            }
        })
        {(failure) in
            
            self.activityView.isHidden = true
            let alert = UIAlertController(title: "Invalid", message: "No internet connection", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title:"Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated:true,completion: nil)
        }
    }
    
    func GetNearMammo()
    {
        let lat = String(currentLocation.latitude)
        let long = String(currentLocation.longitude)
        activityView.isHidden = false
        let userinfo:NSDictionary = [:]
        
        Global().alamofire_withGetMethod("doc/centre/\(lat)/\(long)", parameters: userinfo, completion: { (result) in
            print("Product data",result)
            let msg = (result as? NSDictionary)?.value(forKey: "message") as! String?
            if (result as! NSDictionary).value(forKey: "status") as? Bool == true
            {
                self.activityView.isHidden = true
                 self.mammogramData1 =  (result as! NSDictionary).value(forKey: "doc") as! NSArray
                print("get mammogram data",self.mammogramData1)
                self.NeartableView.reloadData()
            }
            else{
                self.activityView.isHidden = true
                Global().showSimpleAlert(Title: "Hashy", message: msg!, inClass: self)
            }
        })
        {(failure) in
            
            self.activityView.isHidden = true
            let alert = UIAlertController(title: "Invalid", message: "No internet connection", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title:"Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated:true,completion: nil)
        }
    }
   
    func GetTopMammo()
    {
        activityView.isHidden = false
        let userinfo:NSDictionary = [:]
        
        Global().alamofire_withGetMethod("doc/orderByRating/centre", parameters: userinfo, completion: { (result) in
            print("Product data",result)
            let msg = (result as? NSDictionary)?.value(forKey: "message") as! String?
            if (result as! NSDictionary).value(forKey: "status") as? Bool == true
            {
                self.activityView.isHidden = true
                self.mammogramData2 =  (result as! NSDictionary).value(forKey: "doc") as! NSArray
                print("get mammogram data",self.mammogramData2)
                self.MosttableView.reloadData()
            }
            else{
                self.activityView.isHidden = true
                Global().showSimpleAlert(Title: "Hashy", message: msg!, inClass: self)
            }
        })
        {(failure) in
            
            self.activityView.isHidden = true
            let alert = UIAlertController(title: "Invalid", message: "No internet connection", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title:"Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated:true,completion: nil)
        }
    }
}
