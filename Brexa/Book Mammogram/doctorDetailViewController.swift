//
//  doctorDetailViewController.swift
//  Brexa
//
//  Created by Jarvics  on 05/01/18.
//  Copyright © 2018 Omninos_Solutions. All rights reserved.
//

import UIKit

class doctorDetailViewController: UIViewController {

    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var view1: UIView!
    var rate = Double()
    var address = String()
    var about = String()
    var name = String()
    var profile = String()
    var Phone = String()
    
    @IBOutlet weak var editView: UIView!
    @IBOutlet weak var pictureView: UIView!
    @IBOutlet weak var aboutLbl1: UILabel!
    @IBOutlet weak var rateView: UIView!
    @IBOutlet weak var picture: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var aboutData: UITextView!
    @IBOutlet weak var addLbl: UILabel!
    @IBOutlet weak var ratingLbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

       editView.layer.cornerRadius = editView.frame.size.width/2
        editView.clipsToBounds = true
        view1.layer.shadowColor = UIColor.lightGray.cgColor
        view1.layer.shadowOffset = CGSize(width:0,height: 1.5)
        view1.layer.shadowRadius = 2.0
        view1.layer.shadowOpacity = 1.0
        
        view2.layer.shadowColor = UIColor.lightGray.cgColor
        view2.layer.shadowOffset = CGSize(width:0,height: 1.5)
        view2.layer.shadowRadius = 2.0
        view2.layer.shadowOpacity = 1.0
        
        if profile != "" {
            let url = URL(string: profile)!
            print(url)
           picture.af_setImage(withURL: url)
        }
        else{
           picture.image =  UIImage(named: "Mamogram lab")
        }
        
        nameLbl.text = name
        aboutData.text = about
        addLbl.text = address
        
        if rate == 0.0
        {
          rateView.isHidden = true
           
//             self.view1.frame = CGRect(x:self.view1.frame.origin.x,y: self.view1.frame.origin.y - self.rateView.frame.size.height , width:self.view1.frame.size.width,height:self.view1.frame.size.height);
            
            view1.frame.origin.y = self.view1.frame.origin.y - self.rateView.frame.size.height
        }
        else
        {
        let rate1 = String(rate)
        ratingLbl.text = "\(rate1)/5"
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func ratingBtn(_ sender: Any) {
    }
    
    @IBAction func reviewBtn(_ sender: Any) {
    }
    
    @IBAction func editBtn(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "HostptdetailRatingVC") as! HostptdetailRatingVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func callBookBtn(_ sender: Any) {
        
        if let phoneCallURL:URL = URL(string: "tel:\(self.Phone)") {
            let application:UIApplication = UIApplication.shared
            if (application.canOpenURL(phoneCallURL)) {
                let alertController = UIAlertController(title: "Hashy", message: "Are you sure you want to call \n\(self.Phone)?", preferredStyle: .alert)
                let yesPressed = UIAlertAction(title: "Yes", style: .default, handler: { (action) in
                    UIApplication.shared.open(phoneCallURL)
                })
                let noPressed = UIAlertAction(title: "No", style: .default, handler: { (action) in
                    
                })
                alertController.addAction(yesPressed)
                alertController.addAction(noPressed)
                present(alertController, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func backBtn(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }
}
