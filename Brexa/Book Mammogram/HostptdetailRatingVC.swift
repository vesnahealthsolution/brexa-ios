//
//  HostptdetailRatingVC.swift
//  Brexa
//
//  Created by Jarvics on 01/11/17.
//  Copyright © 2017 Omninos_Solutions. All rights reserved.
//

import UIKit

class HostptdetailRatingVC: UIViewController {

    @IBOutlet weak var rate25: UIImageView!
    @IBOutlet weak var rate24: UIImageView!
    @IBOutlet weak var rate23: UIImageView!
    @IBOutlet weak var rate22: UIImageView!
    @IBOutlet weak var rate21: UIImageView!
    @IBOutlet weak var rate20: UIImageView!
    @IBOutlet weak var rate19: UIImageView!
    @IBOutlet weak var rate18: UIImageView!
    @IBOutlet weak var rate17: UIImageView!
    @IBOutlet weak var rate16: UIImageView!
    @IBOutlet weak var rate15: UIImageView!
    @IBOutlet weak var rate14: UIImageView!
    @IBOutlet weak var rate13: UIImageView!
    @IBOutlet weak var rate12: UIImageView!
    @IBOutlet weak var rate11: UIImageView!
    @IBOutlet weak var rate10: UIImageView!
    @IBOutlet weak var rate9: UIImageView!
    @IBOutlet weak var rate8: UIImageView!
    @IBOutlet weak var rate7: UIImageView!
    @IBOutlet weak var rate6: UIImageView!
    @IBOutlet weak var rate5: UIImageView!
    @IBOutlet weak var rate4: UIImageView!
    @IBOutlet weak var rate3: UIImageView!
    @IBOutlet weak var rate2: UIImageView!
    @IBOutlet weak var rate1: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

       self.view.backgroundColor = UIColor(red: 187/255.0, green: 42/255.0, blue: 105/255.0, alpha: 1)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }
    
    @IBAction func Back(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    @IBAction func Submitbtn(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func rate1Btn(_ sender: UIButton) {
      
        if sender.tag == 1
        {
            rate1.image = UIImage(named: "ratestar")
            rate2.image = UIImage(named: "star")
            rate3.image = UIImage(named: "star")
            rate4.image = UIImage(named: "star")
            rate5.image = UIImage(named: "star")
        }
        if sender.tag == 2
        {
            rate1.image = UIImage(named: "ratestar")
            rate2.image = UIImage(named: "ratestar")
            rate3.image = UIImage(named: "star")
            rate4.image = UIImage(named: "star")
            rate5.image = UIImage(named: "star")
        }
        if sender.tag == 3
        {
            rate1.image = UIImage(named: "ratestar")
            rate2.image = UIImage(named: "ratestar")
            rate3.image = UIImage(named: "ratestar")
            rate4.image = UIImage(named: "star")
            rate5.image = UIImage(named: "star")
        }
        if sender.tag == 4
        {
            rate1.image = UIImage(named: "ratestar")
            rate2.image = UIImage(named: "ratestar")
            rate3.image = UIImage(named: "ratestar")
            rate4.image = UIImage(named: "ratestar")
            rate5.image = UIImage(named: "star")
        }
        if sender.tag == 5
        {
            rate1.image = UIImage(named: "ratestar")
            rate2.image = UIImage(named: "ratestar")
            rate3.image = UIImage(named: "ratestar")
            rate4.image = UIImage(named: "ratestar")
            rate5.image = UIImage(named: "ratestar")
        }
        
    }
    
    @IBAction func rate2Btn(_ sender: UIButton) {
        
        if sender.tag == 6
        {
            rate6.image = UIImage(named: "ratestar")
            rate7.image = UIImage(named: "star")
            rate8.image = UIImage(named: "star")
            rate9.image = UIImage(named: "star")
            rate10.image = UIImage(named: "star")
        }
        if sender.tag == 7
        {
            rate6.image = UIImage(named: "ratestar")
            rate7.image = UIImage(named: "ratestar")
            rate8.image = UIImage(named: "star")
            rate9.image = UIImage(named: "star")
            rate10.image = UIImage(named: "star")
        }
        if sender.tag == 8
        {
            rate6.image = UIImage(named: "ratestar")
            rate7.image = UIImage(named: "ratestar")
            rate8.image = UIImage(named: "ratestar")
            rate9.image = UIImage(named: "star")
            rate10.image = UIImage(named: "star")
        }
        if sender.tag == 9
        {
            rate6.image = UIImage(named: "ratestar")
            rate7.image = UIImage(named: "ratestar")
            rate8.image = UIImage(named: "ratestar")
            rate9.image = UIImage(named: "ratestar")
            rate10.image = UIImage(named: "star")
        }
        if sender.tag == 10
        {
            rate6.image = UIImage(named: "ratestar")
            rate7.image = UIImage(named: "ratestar")
            rate8.image = UIImage(named: "ratestar")
            rate9.image = UIImage(named: "ratestar")
            rate10.image = UIImage(named: "ratestar")
        }
    }
    
    @IBAction func rate3Btn(_ sender: UIButton) {
        
        if sender.tag == 11
        {
            rate11.image = UIImage(named: "ratestar")
            rate12.image = UIImage(named: "star")
            rate13.image = UIImage(named: "star")
            rate14.image = UIImage(named: "star")
            rate15.image = UIImage(named: "star")
        }
        if sender.tag == 12
        {
            rate11.image = UIImage(named: "ratestar")
            rate12.image = UIImage(named: "ratestar")
            rate13.image = UIImage(named: "star")
            rate14.image = UIImage(named: "star")
            rate15.image = UIImage(named: "star")
        }
        if sender.tag == 13
        {
            rate11.image = UIImage(named: "ratestar")
            rate12.image = UIImage(named: "ratestar")
            rate13.image = UIImage(named: "ratestar")
            rate14.image = UIImage(named: "star")
            rate15.image = UIImage(named: "star")
        }
        if sender.tag == 14
        {
            rate11.image = UIImage(named: "ratestar")
            rate12.image = UIImage(named: "ratestar")
            rate13.image = UIImage(named: "ratestar")
            rate14.image = UIImage(named: "ratestar")
            rate15.image = UIImage(named: "star")
        }
        if sender.tag == 15
        {
            rate11.image = UIImage(named: "ratestar")
            rate12.image = UIImage(named: "ratestar")
            rate13.image = UIImage(named: "ratestar")
            rate14.image = UIImage(named: "ratestar")
            rate15.image = UIImage(named: "ratestar")
        }
    }
    
    @IBAction func rate4Btn(_ sender: UIButton) {
        
        if sender.tag == 16
        {
            rate16.image = UIImage(named: "ratestar")
            rate17.image = UIImage(named: "star")
            rate18.image = UIImage(named: "star")
            rate19.image = UIImage(named: "star")
            rate20.image = UIImage(named: "star")
        }
        if sender.tag == 17
        {
            rate16.image = UIImage(named: "ratestar")
            rate17.image = UIImage(named: "ratestar")
            rate18.image = UIImage(named: "star")
            rate19.image = UIImage(named: "star")
            rate20.image = UIImage(named: "star")
            
        }
        if sender.tag == 18
        {
            rate16.image = UIImage(named: "ratestar")
            rate17.image = UIImage(named: "ratestar")
            rate18.image = UIImage(named: "ratestar")
            rate19.image = UIImage(named: "star")
            rate20.image = UIImage(named: "star")
        }
        if sender.tag == 19
        {
            rate16.image = UIImage(named: "ratestar")
            rate17.image = UIImage(named: "ratestar")
            rate18.image = UIImage(named: "ratestar")
            rate19.image = UIImage(named: "ratestar")
            rate20.image = UIImage(named: "star")
        }
        if sender.tag == 20
        {
            rate16.image = UIImage(named: "ratestar")
            rate17.image = UIImage(named: "ratestar")
            rate18.image = UIImage(named: "ratestar")
            rate19.image = UIImage(named: "ratestar")
            rate20.image = UIImage(named: "ratestar")
        }
    }
    
    @IBAction func rate5Btn(_ sender: UIButton) {
        
        if sender.tag == 21
        {
            rate21.image = UIImage(named: "ratestar")
            rate22.image = UIImage(named: "star")
            rate23.image = UIImage(named: "star")
            rate24.image = UIImage(named: "star")
            rate25.image = UIImage(named: "star")
        }
        if sender.tag == 22
        {
            rate21.image = UIImage(named: "ratestar")
            rate22.image = UIImage(named: "ratestar")
            rate23.image = UIImage(named: "star")
            rate24.image = UIImage(named: "star")
            rate25.image = UIImage(named: "star")
        }
        if sender.tag == 23
        {
            rate21.image = UIImage(named: "ratestar")
            rate22.image = UIImage(named: "ratestar")
            rate23.image = UIImage(named: "ratestar")
            rate24.image = UIImage(named: "star")
            rate25.image = UIImage(named: "star")
        }
        if sender.tag == 24
        {
            rate21.image = UIImage(named: "ratestar")
            rate22.image = UIImage(named: "ratestar")
            rate23.image = UIImage(named: "ratestar")
            rate24.image = UIImage(named: "ratestar")
            rate25.image = UIImage(named: "star")
        }
        if sender.tag == 25
        {
            rate21.image = UIImage(named: "ratestar")
            rate22.image = UIImage(named: "ratestar")
            rate23.image = UIImage(named: "ratestar")
            rate24.image = UIImage(named: "ratestar")
            rate25.image = UIImage(named: "ratestar")
        }
    }
}
