//
//  BookmamTableViewCell.swift
//  Brexa
//
//  Created by Jarvics on 01/11/17.
//  Copyright © 2017 Omninos_Solutions. All rights reserved.
//

import UIKit

class BookmamTableViewCell: UITableViewCell {
    @IBOutlet weak var mammoImg: UIImageView!
    @IBOutlet weak var mammoName: UILabel!
    @IBOutlet weak var star1: UIImageView!
    @IBOutlet weak var star2: UIImageView!
    @IBOutlet weak var star3: UIImageView!
    @IBOutlet weak var star4: UIImageView!
    @IBOutlet weak var star5: UIImageView!
    
    @IBOutlet weak var mainView: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
