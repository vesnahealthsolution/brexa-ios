//
//  ViewController.swift
//  Brexa
//
//  Created by Jarvics on 28/10/17.
//  Copyright © 2017 Omninos_Solutions. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet var logoimg: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
     logoimg.isHidden = true
        animation_logo()
         
          }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
       
    }
    func animation_logo(){
       UIView.animate(withDuration: 1.7, animations: {
            self.logoimg.isHidden = false
            self.logoimg.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
            self.logoimg.frame = CGRect(x:self.logoimg.frame.origin.x,y:self.logoimg.frame.origin.y - 100 , width : self.logoimg.frame.size.width , height: self.logoimg.frame.size.height)
        }) { (finished) in
            UIView.animate(withDuration: 1, animations: {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "LanguageVC") as! LanguageVC
                self.navigationController?.pushViewController(vc, animated: true)
            })
        }
    }
}

