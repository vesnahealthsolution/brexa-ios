//
//  VideoPlayVC.swift
//  Brexa
//
//  Created by Jarvics on 07/11/17.
//  Copyright © 2017 Omninos_Solutions. All rights reserved.
//

import UIKit

class VideoPlayVC: UIViewController {
  var global = Bool()
    @IBOutlet var IncreaseView: UIView!
    @IBOutlet var Screenimg: UIImageView!
    @IBOutlet var headerView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
self.view.backgroundColor = UIColor(red: 187/255.0, green: 42/255.0, blue: 105/255.0, alpha: 1)
       global = true
    }
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backbtn(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    @IBAction func skipbtn(_ sender: Any) {
        
    }
    @IBAction func Fullscreenbtn(_ sender: Any) {
        if global == true{
        IncreaseView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.size.height)
            Screenimg.image = UIImage(named:"Smallscreen")
            global = false
        }else{
            IncreaseView.frame = CGRect(x: 0, y: self.headerView.frame.origin.y + self.headerView.frame.size.height, width: self.view.frame.width, height: self.view.frame.size.height)
            Screenimg.image = UIImage(named:"Fullscreen")
            global = true
        }
    }
    
    

}
