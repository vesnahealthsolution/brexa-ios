//
//  SignInVC.swift
//  Brexa
//
//  Created by Jarvics on 28/10/17.
//  Copyright © 2017 Omninos_Solutions. All rights reserved.
//

import UIKit
import TextFieldEffects
import SystemConfiguration
class SignInVC: UIViewController {
        
    @IBOutlet weak var fbView: UIView!
    @IBOutlet weak var singInmainView: UIView!
    @IBOutlet var emailtxt: HoshiTextField!
    @IBOutlet weak var activityView: UIView!
    @IBOutlet var Passtxt: HoshiTextField!
    var boolvalue = Bool()
    override func viewDidLoad() {
        super.viewDidLoad()

        activityView.isHidden = true
        fbView.layer.shadowColor = UIColor.lightGray.cgColor
        fbView.layer.shadowOffset = CGSize(width:0,height: 1.5)
        fbView.layer.shadowRadius = 2.0
        fbView.layer.shadowOpacity = 1.0
        singInmainView.layer.shadowColor = UIColor.lightGray.cgColor
        singInmainView.layer.shadowOffset = CGSize(width:0,height: 1.5)
        singInmainView.layer.shadowRadius = 2.0
        singInmainView.layer.shadowOpacity = 1.0
        setStatusBarBackgroundColor(color:  UIColor.white)
    }
    func setStatusBarBackgroundColor(color: UIColor) {
        guard  let statusBar = (UIApplication.shared.value(forKey: "statusBarWindow") as AnyObject).value(forKey: "statusBar") as? UIView else {
            return
        }
        statusBar.backgroundColor = color
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func Signinbtn(_ sender: Any) {
        let providedEmailAddress = emailtxt.text
        
        let isEmailAddressValid = isValidEmailAddress(emailAddressString: providedEmailAddress!)
        
        if emailtxt.text==""{  let alert = UIAlertController(title: "Brexa", message: "Please enter your email", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title:"Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated:true,completion: nil)
        }
        else if Passtxt.text==""{  let alert = UIAlertController(title: "Brexa", message: "Enter your password", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title:"Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated:true,completion: nil)
        }
        else if isEmailAddressValid == false
        {
            let alert = UIAlertController(title: "Brexa", message: "Please enter your valid email", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title:"Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated:true,completion: nil)
        }
        else  {
            signinapi()
       }
    }
    @IBAction func Signupbtn(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignupVC") as! SignupVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func Forgotbtn(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ForgotVC") as! ForgotVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func FBbtn(_ sender: Any) {
    }
    func isValidEmailAddress(emailAddressString: String) -> Bool {
        var returnValue = true
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{3,}"
        do {
            let regex = try NSRegularExpression(pattern: emailRegEx)
            let nsString = emailAddressString as NSString
            let results = regex.matches(in: emailAddressString, range: NSRange(location: 0, length: nsString.length))
            
            if results.count == 0
            {
                returnValue = false
            }
            
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            returnValue = false
        }
        return  returnValue
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
       if self.Passtxt.isEditing == true {
            if range.location == 0 && (string == " ") {
                return false
            }
            
            if range.length + range.location > (self.Passtxt.text?.characters.count)! {
                return false
            }
            let newLength: Int = self.Passtxt.text!.characters.count + (string.characters.count ) - range.length
            return newLength <= 12
        }
        else
        {
            return true
        }
    }
        func signinapi(){
            
            self.activityView.isHidden = false
            let dict14 : NSDictionary = ["email":emailtxt.text!,"password":Passtxt.text!]
            print(dict14)
            Global().alamofire_withMethod("login", parameters: dict14, completion: { (result) in
                print(result)
                self.boolvalue = (result as! NSDictionary).value(forKey: "status")as! Bool
                print(self.boolvalue)
                if (result as! NSDictionary).value(forKey: "message")as! String == "Login Success"
                {
                    let token1 = (result as! NSDictionary).value(forKey:"token")as! String
                    UserDefaults.standard.set(token1, forKey: "authorizationToken")
                     self.activityView.isHidden = true
                    UserDefaults.standard.set(true, forKey: "isUserLoggedIn")
                    UserDefaults.standard.synchronize()
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                        self.navigationController?.pushViewController(vc, animated: true)
                    
                }else{
                    self.activityView.isHidden = true
                    let msg = (result as? NSDictionary)?.value(forKey: "message") as! String?
                    Global().showSimpleAlert(Title: "Brexa", message: msg!, inClass: self)

                }
            }) { (failure) in
                print(failure)
                self.activityView.isHidden = true
                let alert = UIAlertController(title: "Invalid", message: "No internet connection", preferredStyle: UIAlertControllerStyle.alert)
                
                alert.addAction(UIAlertAction(title:"Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated:true,completion: nil)
            }
        }
}

protocol Utilities {
}

extension NSObject:Utilities
{
    enum ReachabilityStatus {
        case notReachable
        case reachableViaWWAN
        case reachableViaWiFi
    }
    
    var currentReachabilityStatus: ReachabilityStatus {
        
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        }) else {
            return .notReachable
        }
        
        var flags: SCNetworkReachabilityFlags = []
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
            return .notReachable
        }
        
        if flags.contains(.reachable) == false {
            // The target host is not reachable.
            return .notReachable
        }
        else if flags.contains(.isWWAN) == true {
            // WWAN connections are OK if the calling application is using the CFNetwork APIs.
            return .reachableViaWWAN
        }
        else if flags.contains(.connectionRequired) == false {
            // If the target host is reachable and no connection is required then we'll assume that you're on Wi-Fi...
            return .reachableViaWiFi
        }
        else if (flags.contains(.connectionOnDemand) == true || flags.contains(.connectionOnTraffic) == true) && flags.contains(.interventionRequired) == false {
            // The connection is on-demand (or on-traffic) if the calling application is using the CFSocketStream or higher APIs and no [user] intervention is needed
            return .reachableViaWiFi
        }
        else {
            return .notReachable
        }
    }
}


