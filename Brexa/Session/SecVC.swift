//
//  SecVC.swift
//  Brexa
//
//  Created by Jarvics on 28/10/17.
//  Copyright © 2017 Omninos_Solutions. All rights reserved.
//

import UIKit

class SecVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
self.perform(#selector(self.dismissViewController), with: self, afterDelay: 0)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func dismissViewController() {
        //if you are pushing your viewControler, then use below single line code
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        self.navigationController?.pushViewController(vc, animated: false)
    }

}
