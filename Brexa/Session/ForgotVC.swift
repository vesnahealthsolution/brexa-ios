//
//  ForgotVC.swift
//  Brexa
//
//  Created by Jarvics on 30/10/17.
//  Copyright © 2017 Omninos_Solutions. All rights reserved.
//

import UIKit
import TextFieldEffects

class ForgotVC: UIViewController {

    @IBOutlet weak var activityView: UIView!
    @IBOutlet var emailtxt: HoshiTextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        setStatusBarBackgroundColor(color:  UIColor(red: 187/255.0, green: 42/255.0, blue: 105/255.0, alpha: 1))
        activityView.isHidden = true
    }
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }
    func setStatusBarBackgroundColor(color: UIColor) {
        guard  let statusBar = (UIApplication.shared.value(forKey: "statusBarWindow") as AnyObject).value(forKey: "statusBar") as? UIView else {
            return
        }
        statusBar.backgroundColor = color
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func Back(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func Submit(_ sender: Any) {
        let providedEmailAddress = emailtxt.text
        
        let isEmailAddressValid = isValidEmailAddress(emailAddressString: providedEmailAddress!)
        
        if emailtxt.text==""{  let alert = UIAlertController(title: "Brexa", message: "Please enter your email", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title:"Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated:true,completion: nil)
        }
        else if isEmailAddressValid == false
        {
            let alert = UIAlertController(title: "Brexa", message: "Please enter your valid email", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title:"Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated:true,completion: nil)
        }
        else  {
            self.activityView.isHidden = false
            let dict14 : NSDictionary = ["email":emailtxt.text!]
            print(dict14)
            Global().alamofire_withMethod("reset_password", parameters: dict14, completion: { (result) in
                print(result)

                if (result as! NSDictionary).value(forKey: "message")as! String == "Email send"
                {
                    self.activityView.isHidden = true
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignInVC") as! SignInVC
                    self.navigationController?.pushViewController(vc, animated: true)
                    
                }else{
                    self.activityView.isHidden = true
                    let msg = (result as? NSDictionary)?.value(forKey: "message") as! String?
                    Global().showSimpleAlert(Title: "Brexa", message: msg!, inClass: self)
                    
                }
            }) { (failure) in
                print(failure)
                self.activityView.isHidden = true
                let alert = UIAlertController(title: "Invalid", message: "No internet connection", preferredStyle: UIAlertControllerStyle.alert)
                
                alert.addAction(UIAlertAction(title:"Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated:true,completion: nil)
            }
        }
        }
    
    func isValidEmailAddress(emailAddressString: String) -> Bool {
        var returnValue = true
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{3,}"
        do {
            let regex = try NSRegularExpression(pattern: emailRegEx)
            let nsString = emailAddressString as NSString
            let results = regex.matches(in: emailAddressString, range: NSRange(location: 0, length: nsString.length))
            
            if results.count == 0
            {
                returnValue = false
            }
            
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            returnValue = false
        }
        return  returnValue
    }
}
