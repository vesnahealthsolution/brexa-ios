//
//  SignupVC.swift
//  Brexa
//
//  Created by Jarvics on 30/10/17.
//  Copyright © 2017 Omninos_Solutions. All rights reserved.
//

import UIKit
import TextFieldEffects
import GoogleMaps
import GooglePlaces
import Alamofire
class SignupVC: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate, GMSMapViewDelegate ,  CLLocationManagerDelegate{
    var imagePicker = UIImagePickerController()
     var imageData21 = Data()
    var zoomLevel = Float()
    var marker = GMSMarker()
    var lat = NSString()
    var lng = NSString()
    var locationManager = CLLocationManager()
    var manager = CLLocationManager()
    var location = CLLocationCoordinate2D()
    var locationStart = CLLocation()
    var locationEnd = CLLocation()
    var currentLocation = CLLocationCoordinate2D()
    var imgarr = NSMutableArray()
    var ageint = Int()
    var dataImage = String()
    var fullBase64String = String()
    var jsonLocation = String()
    var dataImg = String()
    var encodedData = Bool()
    @IBOutlet var GenderView: UIView!
    @IBOutlet var Gendertxt: HoshiTextField!
    @IBOutlet var Agetxt: HoshiTextField!
    @IBOutlet var phntxt: HoshiTextField!
    @IBOutlet var Emailtxt: HoshiTextField!
    @IBOutlet var Passwordtxt: HoshiTextField!
    @IBOutlet var Nametxt: HoshiTextField!
    @IBOutlet var ShdowView: UIView!
    @IBOutlet var BackView: UIView!
    @IBOutlet var thrdView: UIView!
    @IBOutlet var ChooseImage: UIImageView!
    @IBOutlet var CameraLogo: UIImageView!
    @IBOutlet var HeaderView: UIView!
    var str2 = String()
    @IBOutlet weak var activityView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
         setStatusBarBackgroundColor(color:  UIColor(red: 187/255.0, green: 42/255.0, blue: 105/255.0, alpha: 1))
        activityView.isHidden = true
        GenderView.isHidden = true
//        HeaderView.layer.shadowColor = UIColor.lightGray.cgColor
//        HeaderView.layer.shadowOffset = CGSize(width:0,height: 1.5)
//        HeaderView.layer.shadowRadius = 2.0
//        HeaderView.layer.shadowOpacity = 1.0
       
        BackView.layer.shadowColor = UIColor.lightGray.cgColor
        BackView.layer.shadowOffset = CGSize(width:0,height: 1.5)
        BackView.layer.shadowRadius = 2.0
        BackView.layer.shadowOpacity = 1.0
        
        thrdView.layer.shadowColor = UIColor.lightGray.cgColor
        thrdView.layer.shadowOffset = CGSize(width:0,height: 1.5)
        thrdView.layer.shadowRadius = 2.0
        thrdView.layer.shadowOpacity = 1.0
        locationManager.requestWhenInUseAuthorization()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        //Location Manager code to fetch current location
        self.locationManager.delegate = self
        self.locationManager.startUpdatingLocation()
        manager.delegate = self
        manager.requestWhenInUseAuthorization()
        locationManager.requestWhenInUseAuthorization()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.delegate = self
        self.locationManager.startUpdatingLocation()

    }
    
    
    func setStatusBarBackgroundColor(color: UIColor) {
        
        guard let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView else { return }
        
        statusBar.backgroundColor = UIColor(red: 187/255.0, green: 42/255.0, blue: 105/255.0, alpha: 1)
    }
         func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]){
            currentLocation = (manager.location?.coordinate)!
            lat = String(currentLocation.latitude)as NSString
            lng = String(currentLocation.latitude)as NSString
            print(currentLocation)
            print(lat)
            print(lng)
            
            manager.stopUpdatingLocation()
        }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    @IBAction func SignUpBtn(_ sender: Any) {
        
        let providedEmailAddress = Emailtxt.text
        
        let isEmailAddressValid = isValidEmailAddress(emailAddressString: providedEmailAddress!)
        
        if Nametxt.text==""{  let alert = UIAlertController(title: "Brexa", message: "Please enter your name", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title:"Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated:true,completion: nil)
        }
        else if Emailtxt.text==""{  let alert = UIAlertController(title: "Brexa", message: "Please enter your email", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title:"Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated:true,completion: nil)
        }
        else if phntxt.text==""{  let alert = UIAlertController(title: "Brexa", message: "Please enter your Phone", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title:"Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated:true,completion: nil)
        }
        else if Passwordtxt.text==""{  let alert = UIAlertController(title: "Brexa", message: "Please enter your Password", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title:"Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated:true,completion: nil)
        }
        else if Agetxt.text==""{  let alert = UIAlertController(title: "Brexa", message: "Please enter your age", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title:"Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated:true,completion: nil)
            
        }
        else if Gendertxt.text==""{  let alert = UIAlertController(title: "Brexa", message: "Gender can't be empty", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title:"Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated:true,completion: nil)
        }
        else if isEmailAddressValid == false
        {
            let alert = UIAlertController(title: "Invalid", message: "please enter valid email", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title:"Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated:true,completion: nil)
           
        }
        else  {
            //signupapi()
            let strage = Agetxt.text!
            ageint = (strage as NSString).integerValue
            print(ageint)
            let dict = ["lat":lat,"lng":lng]
            
            if let json = try? JSONSerialization.data(withJSONObject: dict, options: []) {
                if String(data: json, encoding: String.Encoding.utf8) != nil {
                    jsonLocation =  String(data: json, encoding: String.Encoding.utf8)!
                    print("json data location",jsonLocation)
                     str2 = jsonLocation.replacingOccurrences(of: "\\", with: "", options: .literal, range: nil)
                    print(str2)
                }
            }
           
            if (currentReachabilityStatus != .notReachable) == true
            {
                if fullBase64String == ""
                {
                    dataImg = ""
                    encodedData = false
                    
                }
                
                else
                {
                    dataImg = fullBase64String
                    encodedData = true
                }
                self.activityView.isHidden = false
                let dict : NSDictionary = ["email":Emailtxt.text!,"password":Passwordtxt.text!,"name":Nametxt.text!,"phone":phntxt.text!,"dob":ageint,"gender":Gendertxt.text!,"location":str2,"profile_pic": dataImg,"encoded":encodedData]
               
                print("Parameters",dict)
                
                Global().alamofire_withMethod("register", parameters: dict, completion: { (result) in
                    print(result)
                    
                    if (result as! NSDictionary).value(forKey: "message") as? String == "Success"
                    {
                        self.activityView.isHidden = true
                        let token1 = (result as! NSDictionary).value(forKey:"token")as! String
                        UserDefaults.standard.set(token1, forKey: "authorizationToken")
                        UserDefaults.standard.set(true, forKey: "isUserLoggedIn")
                        UserDefaults.standard.synchronize()
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                    self.navigationController?.pushViewController(vc, animated: true)
                        
                    }
                    
                    else
                    {     self.activityView.isHidden = true
                         let msg = (result as? NSDictionary)?.value(forKey: "message") as! String?
                       Global().showSimpleAlert(Title: "Brexa", message: msg!, inClass: self)
                    }
                })
                {(failure) in
                    print(failure)
                      self.activityView.isHidden = true
                    let alert = UIAlertController(title: "Brexa", message: "Something went erong please try again..", preferredStyle: UIAlertControllerStyle.alert)
                    
                    alert.addAction(UIAlertAction(title:"Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated:true,completion: nil)
                }
                }
    
            else
            {
                print("No Connection Here")
                self.activityView.isHidden = true
                let alert = UIAlertController(title: "Brexa", message: "Internet Connection error..", preferredStyle: UIAlertControllerStyle.alert)
                
                alert.addAction(UIAlertAction(title:"Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated:true,completion: nil)
            }
        }
    }
    @IBAction func Backbtn(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }
  
    func isValidEmailAddress(emailAddressString: String) -> Bool {
        var returnValue = true
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{3,}"
        do {
            let regex = try NSRegularExpression(pattern: emailRegEx)
            let nsString = emailAddressString as NSString
            let results = regex.matches(in: emailAddressString, range: NSRange(location: 0, length: nsString.length))
            
            if results.count == 0
            {
                returnValue = false
            }
            
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            returnValue = false
        }
        return  returnValue
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField==Nametxt {
            let characterSet = CharacterSet.letters
            
            if string.rangeOfCharacter(from: characterSet.inverted) != nil {
                return false
            }
            return true
        }
        else if textField==phntxt{
            let aSet = NSCharacterSet(charactersIn:"0123456789").inverted
            let compSepByCharInSet = string.components(separatedBy: aSet)
            let numberFiltered = compSepByCharInSet.joined(separator: "")
            return string == numberFiltered
            //3return true
        }
        else if self.Passwordtxt.isEditing == true {
            if range.location == 0 && (string == " ") {
                return false
            }
            
            if range.length + range.location > (self.Passwordtxt.text?.characters.count)! {
                return false
            }
            let newLength: Int = self.Passwordtxt.text!.characters.count + (string.characters.count ) - range.length
            return newLength <= 12
        }
        else
        {
            return true
        }
    }
    @IBAction func GenderViewOpenBtn(_ sender: Any) {
        GenderView.isHidden = false
    }
    @IBAction func MaleClickbtn(_ sender: Any) {
        Gendertxt.text = "Male"
        GenderView.isHidden = true
    }
    @IBAction func GenderClickbtn(_ sender: Any) {
        Gendertxt.text = "Female"
        GenderView.isHidden = true
    }
    
    @IBAction func Ediitprobtn(_ sender: Any) {
        print("imageBtn")
        let actionSheet = UIAlertController.init(title: nil, message: "Pick From", preferredStyle: .actionSheet)
        let actionPhotoCamera = UIAlertAction(title: "Camera", style: .default) { (action) in
            print("cameraCoding")
            self.openCamera()
        }
        let actionLibrary1 = UIAlertAction(title: "Gallery", style: .default) { (action) in
            print("library")
            self.photoLibrary()
        }
        let okey = UIAlertAction(title: "cancel", style: .cancel)  { (action) in
            print("cancel")
        }
        actionSheet.addAction(okey)
        actionSheet.addAction(actionPhotoCamera)
        actionSheet.addAction(actionLibrary1)
        self.present(actionSheet, animated: true, completion: nil)
        
    }
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)){
            
            let myPickerController = UIImagePickerController()
            myPickerController.allowsEditing = false
            myPickerController.delegate = self;
            myPickerController.sourceType = UIImagePickerControllerSourceType.camera
            myPickerController.cameraCaptureMode = .photo
            present(myPickerController, animated: true, completion: nil)
        }else{
            let alert = UIAlertController(title: "Camera Not Found", message: "This device has no Camera", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style:.default, handler: nil)
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
        }
    }
    func photoLibrary()
    {
        let myPickerController = UIImagePickerController()
        myPickerController.delegate = self;
        myPickerController.sourceType = UIImagePickerControllerSourceType.photoLibrary
        self.present(myPickerController, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        imagePicker.sourceType = UIImagePickerControllerSourceType.savedPhotosAlbum;
        ChooseImage.image = chosenImage
        self.resizeImage(chosenImage, newHeight: 480)
        dataImage = imageData21.base64EncodedString(options: .lineLength64Characters)
        //fullBase64String = "data:image/png;base64,\(dataImage))"
        fullBase64String = dataImage
        //print( fullBase64String)
        CameraLogo.isHidden = true
        
        dismiss(animated: true, completion: nil)
    }
   
    func signupapi1(){
        let strage = Agetxt.text!
        ageint = (strage as NSString).integerValue
        print(ageint)
        let jsonobject: [String: AnyObject] = ["lat":30.455 as AnyObject,"lng":70.548 as AnyObject]
        print(jsonobject)
        
        let dict14 : NSDictionary = ["email":Emailtxt.text!,"password":Passwordtxt.text!,"name":Nametxt.text!,"phone":phntxt.text!,"dob":ageint,"gender":Gendertxt.text!,"location":jsonobject,"profile_pic":Global().generateBase64ImageString(image: ChooseImage.image!),"encoded":"true"]
        print(dict14)
        Global().alamofire_withMethod("register", parameters: dict14, completion: { (result) in
            print(result)
        }) { (failure) in
    
        print(failure)
        }
    
    }
    
    func signUpApi()
    {
    }
    
    func resizeImage(_ image: UIImage, newHeight: CGFloat) -> UIImage {
        let scale = newHeight / image.size.height
        let newWidth = image.size.width * scale
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        image.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        imageData21 = UIImageJPEGRepresentation(newImage!, 0.5)!  as Data
        UIGraphicsEndImageContext()
        return UIImage(data:imageData21 as Data)!
    }
}
