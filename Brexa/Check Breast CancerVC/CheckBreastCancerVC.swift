//
//  CheckBreastCancerVC.swift
//  Brexa
//
//  Created by Jarvics on 07/11/17.
//  Copyright © 2017 Omninos_Solutions. All rights reserved.
//

import UIKit

class CheckBreastCancerVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        setStatusBarBackgroundColor(color: UIColor(red: 187/255.0, green: 42/255.0, blue: 105/255.0, alpha: 1))
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func backbtn(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    func setStatusBarBackgroundColor(color: UIColor) {
        guard  let statusBar = (UIApplication.shared.value(forKey: "statusBarWindow") as AnyObject).value(forKey: "statusBar") as? UIView else {
            return
        }
        statusBar.backgroundColor = color
    }
    

  
}
